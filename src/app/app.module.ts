import { NgModule, NO_ERRORS_SCHEMA } from "@angular/core";
import { NativeScriptModule } from "nativescript-angular/nativescript.module";
import { AppRoutingModule } from "./app-routing.module";
import { AppComponent } from "./app.component";
import { NativeScriptCommonModule } from "nativescript-angular/common";
import { NativeScriptFormsModule } from "nativescript-angular/forms";
import { NativeScriptHttpClientModule } from "nativescript-angular/http-client";
import { LoginComponent } from "./login/login.component";
import { PwdComponent } from "./passwordreset/passwordreset.component";
import { VerifyComponent } from "./passwordreset/verify.component";
import { ActchgeComponent } from"./passwordreset/actchge.component";
import { VerifyEmailComponent } from "./signup/verifyemail.component";
import { SignupComponent } from"./signup/signup.component";
import { ProfilepicComponent } from "./signup/profilepicture.component";
import { HomePageComponent } from "./home/home.component";
import { MapsComponent } from "./maps/maps.component";
import { HomeService } from "./home.service";
import { TimeStr } from "./timecalc.pipe";
import { Adress } from "./ip.service";
import { NativeScriptUIListViewModule } from "nativescript-ui-listview/angular";
import { TNSImageModule } from 'nativescript-image/angular';
import { ErrorPageComponent } from"./error/error.component";
import { InternetPageComponent } from"./error/internet.component";
import * as imageModule from 'nativescript-image';
import { TNSPlayer } from 'nativescript-audio';
import { Video } from 'nativescript-videoplayer';
import * as applicationModule from 'tns-core-modules/application';
if (applicationModule.android) {
    applicationModule.on(applicationModule.launchEvent, () => {
        console.log('initialize pipeline');
        imageModule.initialize({isDownsampleEnabled: true});
    });
}
@NgModule({
    bootstrap: [
        AppComponent
    ],
    imports: [
        TNSImageModule,
        NativeScriptUIListViewModule,
        NativeScriptModule,
        AppRoutingModule,
        NativeScriptCommonModule,
        NativeScriptFormsModule,
        NativeScriptHttpClientModule,
    ],
    declarations: [
        InternetPageComponent,
        ErrorPageComponent,
        AppComponent,
        ProfilepicComponent,
        VerifyComponent,
        LoginComponent,
        VerifyEmailComponent,
        ActchgeComponent,
        PwdComponent,
        SignupComponent,
        HomePageComponent,
        MapsComponent,
        TimeStr,
    ],
    providers: [
      TNSPlayer,
      HomePageComponent,
      Adress,
      HomeService,
      Video,
    ],
    schemas: [
        NO_ERRORS_SCHEMA
    ]
})
/*
Pass your application module to the bootstrapModule function located in main.ts to start your app
*/
export class AppModule { }
