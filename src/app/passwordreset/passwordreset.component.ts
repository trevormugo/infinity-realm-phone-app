import { Component, OnInit } from "@angular/core";
import * as dialog from "tns-core-modules/ui/dialogs";
import { Page } from "tns-core-modules/ui/page";
import { LoginService } from "../login.service";
import { Adress } from "../ip.service";
import { RouterExtensions } from "nativescript-angular/router";
import { TNSFancyAlert, TNSFancyAlertButton } from "nativescript-fancyalert";
import * as appSettings from "tns-core-modules/application-settings";

@Component({
    selector: "ns-pwd",
    providers: [LoginService , Adress],
    moduleId: module.id,
    templateUrl: "./passwordreset.component.html",
    styleUrls: ["./passwordreset.component.css"],
})
export class PwdComponent implements OnInit {
    public email:String;
    public activity:Boolean = false;
    public showbtn:Boolean = true;
    ngOnInit() {
        this.page.actionBarHidden = true;
    }
    constructor(public routerextensions : RouterExtensions, public page : Page , public logingin: LoginService) {}
    //btn click
    public backtologin(){
      if(this.activity === true)
      {
        TNSFancyAlert.showInfo(
          "info!",
          "wait till current process is finished.",
          "close"
        ).then(() => {

        });
      }
      else
      {
        this.routerextensions.back();
      }
    }
    public verification(){
      if(!this.email)
      {
        TNSFancyAlert.showError(
          "Error!",
          "please provide an email.",
          "close"
        ).then(() => {
             console.log("user pressed the button");
        });
      }
      else
      {
        this.showbtn = false;
        this.activity = true;
        const mydata = {
          email : this.email,
        };
        this.logingin.send(mydata)
            .subscribe((res) => {
              if( res.message === "err")
              {
                //an error occured while trying to verify the existence of the email
                TNSFancyAlert.showError(
                  "Error!",
                  "an error occured while trying to verify the existence of the email.",
                  "close"
                ).then(() => {
                     this.showbtn = true;
                     this.activity = false;
                });
              }
              else if(!res){
                //no need for the .error since its the only response with an identifier of error;
                //an error occured in finding the information from the database
                TNSFancyAlert.showError(
                  "Error!",
                  "there was an unexpected error try again in a bit.",
                  "close"
                ).then(() => {
                     this.showbtn = true;
                     this.activity = false;
                });
              }
              else if(res.message === "noemail")
              {
                //no email exists
                TNSFancyAlert.showError(
                  "Error!",
                  "the email does not exist.",
                  "close"
                ).then(() => {
                     this.showbtn = true;
                     this.activity = false;
                });
              }
              else if(res.message === "error")
              {
                //the code was not delivred to the email try again
                TNSFancyAlert.showError(
                  "Error!",
                  "the email was not delivered.",
                  "close"
                ).then(() => {
                     this.showbtn = true;
                     this.activity = false;
                });
              }
              else
              {
                //the code was sent
                var stringed = res.message.toString();
                var email = this.email.toString();
                appSettings.setString("code", stringed );
                appSettings.setString("email", email);
                TNSFancyAlert.showSuccess(
                  "Success",
                  "the code was successfuly delivered to the email provided.",
                  "close"
                ).then(() => {
                      this.showbtn = true;
                      this.activity = false;
                     this.routerextensions.navigate(['/verify'], {
                       transition: {
                         name: 'slideLeft',
                         duration: 1000,
                         curve: 'easeOut'
                       }
                     });
                });
              }
            },(error) => {
              TNSFancyAlert.showError(
                "Error!",
                "An Error occured try again later.",
                "close"
              ).then(() => {
                   this.showbtn = true;
                   this.activity = false;
              });
            });
      }
    }




}
