import { Component, OnInit } from "@angular/core";
import * as dialog from "tns-core-modules/ui/dialogs";
import { Page } from "tns-core-modules/ui/page";
import { Adress } from "../ip.service";
import { LoginService } from "../login.service";
import { RouterExtensions } from "nativescript-angular/router";
import * as appSettings from "tns-core-modules/application-settings";
import { TNSFancyAlert, TNSFancyAlertButton } from "nativescript-fancyalert";

@Component({
    selector: "ns-verify",
    providers: [Adress , LoginService],
    moduleId: module.id,
    templateUrl: "./actchge.component.html",
    styleUrls: ["./actchge.component.css"],
})
export class ActchgeComponent implements OnInit {
    public activity:Boolean= false;
    public newpass:String;
    public confirmnewpass:String;
    ngOnInit() {
        this.page.actionBarHidden = true;
        appSettings.remove("code");
        //now there shouldnt be anything in the application settings.
    }
    constructor(public login : LoginService , public routerextensions : RouterExtensions, public page : Page ) {}
    public changepass(){
      if(this.activity === true)
      {
        //wait
        TNSFancyAlert.showInfo(
          "Info!",
          "Please wait until the current process is finished.",
          "close"
        ).then(() => {
             console.log("user pressed the button");
        });
      }
      else
      {
        if( !this.newpass || !this.confirmnewpass )
        {
          TNSFancyAlert.showError(
            "Error!",
            "fill in the empty text field.",
            "close"
          ).then(() => {
               console.log("user pressed the button");
          });
        }
        else
        {
          var myimport = appSettings.getString("email");
          const mynewdata = {
            email : myimport,
            password : this.newpass
          };
          this.activity = true;
          if( this.newpass !== this.confirmnewpass )
          {
            //show error
            TNSFancyAlert.showError(
              "Error!",
              "youre passwords do not match.",
              "close"
            ).then(() => {
              this.activity = false;
            });
          }
          else
          {
            this.login.changepass(mynewdata)
                .subscribe((res)=>{
                  if(res.error === "notsaved")
                  {
                    TNSFancyAlert.showError(
                      "Error!",
                      "An error occured the password was not updated try again later.",
                      "close"
                    ).then(() => {
                      this.activity = false;
                    });
                  }
                  else if(!res){
                    //no need for the .error since its the only response with an identifier of error;
                    //an error occured in finding the information from the database
                    TNSFancyAlert.showError(
                      "Error!",
                      "there was an unexpected error try again in a bit.",
                      "close"
                    ).then(() => {
                         this.activity = false;
                    });
                  }
                  else if(res.message === "changed")
                  {
                    TNSFancyAlert.showSuccess(
                      "Success",
                      "the password was successfuly updated now login with youre new password.",
                      "close"
                    ).then(() => {
                      this.activity = false;
                      this.routerextensions.navigate([''], {
                        clearHistory : true,
                        transition: {
                          name: 'slideTop',
                          duration: 2000,
                          curve: 'spring'
                        }
                      });
                    });
                  }
                },(error) =>{
                  TNSFancyAlert.showError(
                    "Error!",
                    "An Error occured try again later.",
                    "close"
                  ).then(() => {
                    this.activity = false;
                  });
                });
          }
        }
      }
    }
}
