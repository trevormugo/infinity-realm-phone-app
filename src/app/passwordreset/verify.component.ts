import { Component, OnInit } from "@angular/core";
import * as dialog from "tns-core-modules/ui/dialogs";
import { Page } from "tns-core-modules/ui/page";
import { Adress } from "../ip.service";
import { RouterExtensions } from "nativescript-angular/router";
import * as appSettings from "tns-core-modules/application-settings";
import { TNSFancyAlert, TNSFancyAlertButton } from "nativescript-fancyalert";

@Component({
    selector: "ns-verify",
    providers: [Adress],
    moduleId: module.id,
    templateUrl: "./verify.component.html",
    styleUrls: ["./verify.component.css"],
})
export class VerifyComponent implements OnInit {
    public verifycode:String;
    public activity:Boolean = false;
    public showbtn:Boolean = true;
    public counter = 0;
    ngOnInit() {
        this.page.actionBarHidden = true;
        //clear catche in application settings
        appSettings.remove("email");
        var codeisthere = appSettings.getString("code");
        if(codeisthere)
        {
          //donothing
        }
        else
        {
          TNSFancyAlert.showInfo(
            "info!",
            "go back and provide a valid email for validation.",
            "close"
          ).then(() => {
            this.routerextensions.back();
          });
        }
    }
    constructor(public routerextensions : RouterExtensions, public page : Page ) {}
    //btn click
    public resendpage(){
      if(this.activity === true)
      {
        TNSFancyAlert.showInfo(
          "info!",
          "wait till current process is finished.",
          "close"
        ).then(() => {

        });
      }
      else
      {
        this.routerextensions.back();
      }
    }
    public changepasspge(){
      if(!this.verifycode)
      {
        TNSFancyAlert.showError(
          "Error!",
          "fill in the empty text field.",
          "close"
        ).then(() => {
             console.log("user pressed the button");
        });
      }
      else
      {
        const mycode = appSettings.getString("code");
        this.showbtn = false;
        this.activity = true;
        if( this.verifycode !== mycode )
        {
          TNSFancyAlert.showError(
            "Error!",
            "incorrect code.",
            "close"
          ).then(() => {
               var threshhold = 5;
               this.counter = this.counter + 1;
               var attempt = threshhold - this.counter;
               if(attempt === 0 || attempt < 1)
               {
                 this.routerextensions.back();
               }
               this.showbtn = true;
               this.activity = false;
               TNSFancyAlert.showInfo(
                 "Info!",
                 attempt + " attempts left.",
                 "close"
               ).then(() => {

               });
          });
        }
        else
        {
          TNSFancyAlert.showSuccess(
            "Success",
            "correct code.",
            "close"
          ).then(() => {
               this.showbtn = true;
               this.activity = false;
               this.routerextensions.navigate(['/actchge'], {
                 clearHistory : false,
                 transition: {
                   name: 'slideLeft',
                   duration: 1000,
                   curve: 'easeOut'
                 }
               });
          });
        }
      }
    }
}
