import { Component, OnInit } from "@angular/core";
import * as dialog from "tns-core-modules/ui/dialogs";
import { Page } from "tns-core-modules/ui/page";
import { LoginService } from "../login.service";
import { Adress } from "../ip.service";
import { RouterExtensions } from "nativescript-angular/router";
import { ModalDialogParams } from 'nativescript-angular/modal-dialog';
import { TNSFancyAlert, TNSFancyAlertButton } from "nativescript-fancyalert";
import * as app from "tns-core-modules/application";
import * as appSettings from "tns-core-modules/application-settings";
import { HomeService } from "../home.service";
import { LocalNotifications } from "nativescript-local-notifications";
const permissions = require('nativescript-permissions');
import {
  resumeEvent,
  suspendEvent,
  ApplicationEventData,
  on as applicationOn,
  run as applicationRun } from "tns-core-modules/application";
import * as application from "application";

declare var android: any; // <- important! avoids namespace issues
@Component({
    selector: "ns-login",
    providers: [HomeService , LoginService , Adress],
    moduleId: module.id,
    templateUrl: "./login.component.html",
    styleUrls: ["./login.component.css"],
})
export class LoginComponent implements OnInit {
    public activity:Boolean = false;
    public showbtn:Boolean = true;
    public email:String;
    public password:String;
    public yt;
    ngOnInit() {
        this.page.actionBarHidden = true;
        this.askfirst();
        LocalNotifications.requestPermission().then(
            function(granted) {
              console.log("Permission granted? " + granted);
            }
        )
        if(!permissions.hasPermission(android.Manifest.permission.INTERNET)){
          this.askforpermission();
        }
        var mycache = appSettings.getString("mycode");
        var isloggedin = appSettings.getString("idr");
        this.showbtn = false;
        this.activity = true;
        if(isloggedin)
        {
          //confirm idr
          const dataid = {
            data : isloggedin,
          }
          this.homeservice.scanidr(dataid)
                  .subscribe((res)=>{
                    if(res.err === "error")
                    {
                      //donothing
                      this.showbtn = true;
                      this.activity = false;
                      this.routerextensions.navigate(['/home'], {
                        clearHistory: true,
                        transition: {
                          name: 'slideLeft',
                          duration: 900,
                          curve: 'easeOut'
                        }
                      });
                    }
                    else if(res.msg === "real")
                    {
                      //do something
                      this.showbtn = true;
                      this.activity = false;
                      this.routerextensions.navigate(['/home'], {
                        clearHistory: true,
                        transition: {
                          name: 'slideLeft',
                          duration: 900,
                          curve: 'easeOut'
                        }
                      });
                    }
                    else if(res.msg === "fake")
                    {
                      TNSFancyAlert.showInfo(
                        "Info!",
                        "You were previously logged in but there was a problem with you're identifier.",
                        "close"
                      ).then(() => {
                        appSettings.remove("idr");
                        this.showbtn = true;
                        this.activity = false;
                      });
                    }
                    else
                    {
                      //there isnt even nothing do nothing ok?
                      this.showbtn = true;
                      this.activity = false;
                      this.routerextensions.navigate(['/home'], {
                        clearHistory: true,
                        transition: {
                          name: 'slideLeft',
                          duration: 900,
                          curve: 'easeOut'
                        }
                      });
                    }
                  },(error)=>{
                    //do nothing
                    this.showbtn = true;
                    this.activity = false;
                    this.routerextensions.navigate(['/home'], {
                      clearHistory: true,
                      transition: {
                        name: 'slideLeft',
                        duration: 900,
                        curve: 'easeOut'
                      }
                    });
                  });
        }
        else
        {
          this.showbtn = true;
          this.activity = false;
        }

    }
    constructor(public homeservice : HomeService , public routerextensions : RouterExtensions, public page : Page , public logingin: LoginService) {
      applicationOn(suspendEvent, (args: ApplicationEventData) => {
          // args.android is an android activity
          if (args.android) {
              console.log("SUSPEND Activity: " + args.android);
          }
      });

      applicationOn(resumeEvent, (args: ApplicationEventData) => {
          if (args.android) {
              console.log("RESUME Activity: " + args.android);
              let window = app.android.startActivity.getWindow();
              window.setSoftInputMode(
                android.view.WindowManager.LayoutParams.SOFT_INPUT_ADJUST_PAN
              );
              // This can be SOFT_INPUT_ADJUST_PAN
              // Or SOFT_INPUT_ADJUST_RESIZE
          }
      });
    }
    public askforpermission(){
      permissions.requestPermission(android.Manifest.permission.INTERNET, "I need these permissions for the user to be able to access local music from the platform")
      .then( () => {
        console.log("permission granted");
      })
      .catch( () => {
        console.log("no permission");
        TNSFancyAlert.showError(
          "Error!",
          "We need you to accept this request for you to login.",
          "close"
        ).then(() => {
          this.askforpermission();
        });
      });
    }
    public askfirst(){
      permissions.requestPermission(android.Manifest.permission.READ_EXTERNAL_STORAGE, "I need these permissions for the user to be able to access local music from the platform")
      .then( () => {
        console.log("permission granted");
      })
      .catch( () => {
        console.log("no permission");
        TNSFancyAlert.showError(
          "Error!",
          "We need you to accept this request for you to be able to access youre local music.",
          "close"
        ).then(() => {
          this.askfirst();
        });
      });
    }
    //btn click
    public login(){
      if(!this.email || !this.password)
      {
        TNSFancyAlert.showError(
          "Error!",
          "Please fill in all the respective fields.",
          "close"
        ).then(() => {
             console.log("user pressed the button");
        });
      }
      else
      {
        this.showbtn = false;
        this.activity = true;
        const data = {
          email : this.email,
          password : this.password,
        }
        this.logingin.loginReq(data)
              .subscribe((res)=>{
                if(res.error)
                {
                  //no need for the .error since its the only response with an identifier of error;
                  //an error occured in finding the information from the database
                  TNSFancyAlert.showError(
                    "Error!",
                    "there was an unexpected error try again in a bit.",
                    "close"
                  ).then(() => {
                       this.showbtn = true;
                       this.activity = false;
                  });
                }
                else if(!res){
                  //no need for the .error since its the only response with an identifier of error;
                  //an error occured in finding the information from the database
                  TNSFancyAlert.showError(
                    "Error!",
                    "there was an unexpected error try again in a bit.",
                    "close"
                  ).then(() => {
                       this.showbtn = true;
                       this.activity = false;
                  });
                }
                else if(res.message === "noemail")
                {
                  //there was no email in the database
                  TNSFancyAlert.showError(
                    "Error!",
                    "the email you inputed does not exist.",
                    "close"
                  ).then(() => {
                       this.showbtn = true;
                       this.activity = false;
                  });
                }
                else if(res.message === "incorrectpass")
                {
                  //password is incorrect
                  TNSFancyAlert.showError(
                    "Error!",
                    "incorrect password.",
                    "close"
                  ).then(() => {
                       this.showbtn = true;
                       this.activity = false;
                  });
                }
                else if(!res)
                {
                  //the was no data
                  TNSFancyAlert.showError(
                    "Error!",
                    "empty response try again in a bit.",
                    "close"
                  ).then(() => {
                       this.showbtn = true;
                       this.activity = false;
                  });
                }
                else
                {
                  //BUT THERE MIGHT BE OTHER POSSIBLE ERRORS
                  //email exists and the password is correct
                  TNSFancyAlert.showSuccess(
                    "Success!",
                    "correct information "+res.id,
                    "close"
                  ).then(() => {
                       this.showbtn = true;
                       this.activity = false;
                       //navigate to the home page
                       var id = res.id.toString();
                       appSettings.setString("idr",res.id);
                       this.routerextensions.navigate(['/home'], {
                         clearHistory: true,
                         transition: {
                           name: 'slideLeft',
                           duration: 900,
                           curve: 'easeOut'
                         }
                       });
                  });
                }
              },(error)=>{
                TNSFancyAlert.showError(
                  "Error!",
                  "An Error occured try again later.",
                  "close"
                ).then(() => {
                     this.showbtn = true;
                     this.activity = false;
                });
              });
      }
    }
    public tosignup(){
      if(this.activity === true)
      {
        TNSFancyAlert.showInfo(
          "info!",
          "wait till current process is finished.",
          "close"
        ).then(() => {

        });
      }
      else
      {
        this.routerextensions.navigate(['/signup'], {
          transition: {
            name: 'slideTop',
            duration: 2000,
            curve: 'spring'
          }
        });
      }
    }
    public toreset(){
      if(this.activity === true)
      {
        TNSFancyAlert.showInfo(
          "info!",
          "wait till current process is finished.",
          "close"
        ).then(() => {

        });
      }
      else
      {
        this.routerextensions.navigate(['/pwd'], {
          transition: {
            name: 'slideBottom',
            duration: 2000,
            curve: 'spring'
          }
        });
      }
    }
}
