import { Injectable } from "@angular/core";
import * as appSettings from "tns-core-modules/application-settings";
import { HttpClient , HttpHeaders} from "@angular/common/http";
import { Observable , throwError } from "rxjs";
import { retry ,catchError } from "rxjs/operators";
import { Adress } from "./ip.service";
import { LocalNotifications } from "nativescript-local-notifications";
//import { TNSPlayer } from 'nativescript-audio';
declare var android: any; // <- important! avoids namespace issues
const SocketIO = require('nativescript-socket.io');
const options = {
  query: {
    token: 'SOME_TOKEN_HERE',
  },
  android: {
    // http://socketio.github.io/socket.io-client-java/apidocs/io/socket/client/IO.Options.html
  },
  ios: {
    // https://nuclearace.github.io/Socket.IO-Client-Swift/Enums/SocketIOClientOption.html
  }
};
@Injectable()
export class HomeService {
  url = this.outurl.getIp();
  public myid = appSettings.getString("idr");
  public messagesfromservice = [];
  public notificationsfromservice = [];
  public socketio = SocketIO.connect(this.url, options);
  constructor(private http : HttpClient , public outurl : Adress){
    //receive like notifications in realtime
    this.socketio.on('newlikeevent', (message) => {
      if(message.senderid !== message.recipientid)
      {
        if(message.recipientid === this.myid)
        {
          this.notificationsfromservice.unshift(message);
          if(!LocalNotifications.hasPermission())
          {
            LocalNotifications.requestPermission().then(
                function(granted) {
                  console.log("Permission granted? " + granted);
                }
            )
          }
          LocalNotifications.schedule([{
              id: 1, // generated id if not set
              title: message.senderusername,
              body: 'liked youre upload.',
              image: this.url + "/coverimage/" + message.uploadid,
              thumbnail: true,
              at: new Date(new Date().getTime() + (10 * 1000)) // 10 seconds from now
          }]).then(
            function(scheduledIds) {
                console.log("Notification id(s) scheduled: " + JSON.stringify(scheduledIds));
            },
            function(error) {
                console.log("scheduling error: " + error);
            }
          )
        }
      }
      else
      {
        console.log("thats you");
      }
      });
    //save events
    this.socketio.on('newsaveevent', (message) => {
        if(message.senderid !== message.recipientid)
        {
          if(message.recipientid === this.myid)
          {
            this.notificationsfromservice.unshift(message);
            if(!LocalNotifications.hasPermission())
            {
              LocalNotifications.requestPermission().then(
                  function(granted) {
                    console.log("Permission granted? " + granted);
                  }
              )
            }
            LocalNotifications.schedule([{
                id: 1, // generated id if not set
                title: message.senderusername,
                body: 'saved youre upload.',
                image: this.url + "/coverimage/" + message.uploadid,
                thumbnail: true,
                at: new Date(new Date().getTime() + (10 * 1000)) // 10 seconds from now
            }]).then(
              function(scheduledIds) {
                  console.log("Notification id(s) scheduled: " + JSON.stringify(scheduledIds));
              },
              function(error) {
                  console.log("scheduling error: " + error);
              }
            )
          }
        }
        else
        {
          console.log("thats you");
        }
        });
    //recieve follow notifications in realtime
    this.socketio.on('newfollowevent', (message) => {
        if(message.senderid !== message.recipientid)
        {
          if(message.recipientid === this.myid)
          {
            this.notificationsfromservice.unshift(message);
            if(!LocalNotifications.hasPermission())
            {
              LocalNotifications.requestPermission().then(
                  function(granted) {
                    console.log("Permission granted? " + granted);
                  }
              )
            }
            LocalNotifications.schedule([{
                id: 2, // generated id if not set
                title: message.followername,
                body: 'started following you.',
                image: this.url + "/profilenoneupload/" + message.senderid,
                thumbnail: true,
                at: new Date(new Date().getTime() + (10 * 1000)) // 10 seconds from now
            }]).then(
              function(scheduledIds) {
                  console.log("Notification id(s) scheduled: " + JSON.stringify(scheduledIds));
              },
              function(error) {
                  console.log("scheduling error: " + error);
              }
            )
          }
        }
        else
        {
          console.log("thats you");
        }
    });
    //receive text messages in realtime
    this.socketio.on('newmessageevent', (message) => {
      this.messagesfromservice.push(message);
      if(message.senderid !== message.recipientid)
      {
        if(message.recipientid === this.myid)
        {
          if(!LocalNotifications.hasPermission())
          {
            LocalNotifications.requestPermission().then(
                function(granted) {
                  console.log("Permission granted? " + granted);
                }
            )
          }
          LocalNotifications.schedule([{
              id: 3, // generated id if not set
              title:"new message from "  + message.senderusername,
              body: message.message,
              at: new Date(new Date().getTime() + (10 * 1000)) // 10 seconds from now
          }]).then(
            function(scheduledIds) {
                console.log("Notification id(s) scheduled: " + JSON.stringify(scheduledIds));
            },
            function(error) {
                console.log("scheduling error: " + error);
            }
          )
        }
      }
      else
      {
        console.log("thats you");
      }
      });
  }

  //LET US SCAN THE IDR
  scanidr(idinfo):Observable <any> {
    const httpOptions = {
         headers : new HttpHeaders({
            "Content-Type":  "application/json",
            "Authorization": "trevor",
            "Access-Control-Allow-Origin" : "*"
         })
     }
    return this.http.post<any>(this.url + "/idscan",idinfo,httpOptions)
      .pipe(
        retry(3),
      );
  }

  //LET US Fetch THE IDR
  fetchinfo(idinfo):Observable <any> {
    const httpOptions = {
         headers : new HttpHeaders({
            "Content-Type":  "application/json",
            "Authorization": "trevor",
            "Access-Control-Allow-Origin" : "*"
         })
     }
    return this.http.post<any>(this.url + "/fetcher",idinfo,httpOptions)
      .pipe(
        retry(3),
      );
  }

  //LET US FETCH SOME METADATA
  getmetadata(metaid):Observable <any> {
    const httpOptions = {
         headers : new HttpHeaders({
            "Content-Type":  "application/json",
            "Authorization": "trevor",
            "Access-Control-Allow-Origin" : "*"
         })
     }
    return this.http.post<any>(this.url + "/metadata",metaid,httpOptions)
      .pipe(
        retry(3),
      );
  }

  //LET US FETCH SOME FOLLOWERS
  getfollowers(id):Observable <any> {
    const httpOptions = {
         headers : new HttpHeaders({
            "Content-Type":  "application/json",
            "Authorization": "trevor",
            "Access-Control-Allow-Origin" : "*"
         })
     }
    return this.http.post<any>(this.url + "/followerscount",id,httpOptions)
      .pipe(
        retry(3),
      );
  }

  //LET US FETCH SOME FOLLOWERS
  getfollowing(id):Observable <any> {
    const httpOptions = {
         headers : new HttpHeaders({
            "Content-Type":  "application/json",
            "Authorization": "trevor",
            "Access-Control-Allow-Origin" : "*"
         })
     }
    return this.http.post<any>(this.url + "/followingcount",id,httpOptions)
      .pipe(
        retry(3),
      );
  }
  //LET US GET USER SPECIFIC POSTS
  userspecificpost(idspec):Observable <any> {
    const httpOptions = {
         headers : new HttpHeaders({
            "Content-Type":  "application/json",
            "Authorization": "trevor",
            "Access-Control-Allow-Origin" : "*"
         })
     }
    return this.http.post<any>(this.url + "/userspecificpost",idspec,httpOptions)
      .pipe(
        retry(3),
      );
  }

  //LET GET EXTRA TO KNOW IF USER LIKED COMMENTED OR FOLLOWED A PARTICULAR PERSON
  getextra(idspec):Observable <any> {
    const httpOptions = {
         headers : new HttpHeaders({
            "Content-Type":  "application/json",
            "Authorization": "trevor",
            "Access-Control-Allow-Origin" : "*"
         })
     }
    return this.http.post<any>(this.url + "/fetchextrasummary",idspec,httpOptions)
      .pipe(
        retry(3),
      );
  }

  //LET GET EXTRA TO KNOW IF USER LIKED COMMENTED OR FOLLOWED A PARTICULAR PERSON
  getalllikeddata(idspec):Observable <any> {
    const httpOptions = {
         headers : new HttpHeaders({
            "Content-Type":  "application/json",
            "Authorization": "trevor",
            "Access-Control-Allow-Origin" : "*"
         })
     }
    return this.http.post<any>(this.url + "/getlikeddata",idspec,httpOptions)
      .pipe(
        retry(3),
      );
  }
  //route to get all notifications
  getnotifications(idspec):Observable <any> {
    const httpOptions = {
         headers : new HttpHeaders({
            "Content-Type":  "application/json",
            "Authorization": "trevor",
            "Access-Control-Allow-Origin" : "*"
         })
     }
    return this.http.post<any>(this.url + "/getnotifications",idspec,httpOptions)
      .pipe(
        retry(3),
      );
  }
  //route to get all messages
  getmessages(idspec):Observable <any> {
    const httpOptions = {
         headers : new HttpHeaders({
            "Content-Type":  "application/json",
            "Authorization": "trevor",
            "Access-Control-Allow-Origin" : "*"
         })
     }
    return this.http.post<any>(this.url + "/getmessages",idspec,httpOptions)
      .pipe(
        retry(3),
      );
  }
  //route to get act messages
  getactmessages(idspec):Observable <any> {
    const httpOptions = {
         headers : new HttpHeaders({
            "Content-Type":  "application/json",
            "Authorization": "trevor",
            "Access-Control-Allow-Origin" : "*"
         })
     }
    return this.http.post<any>(this.url + "/getactmessages",idspec,httpOptions)
      .pipe(
        retry(3),
      );
  }
  //route to get summary profilepage
  getsummary(idspec):Observable <any> {
    const httpOptions = {
         headers : new HttpHeaders({
            "Content-Type":  "application/json",
            "Authorization": "trevor",
            "Access-Control-Allow-Origin" : "*"
         })
     }
    return this.http.post<any>(this.url + "/profileroutesummary",idspec,httpOptions)
      .pipe(
        retry(3),
      );
  }
  //route to get summary profilepage
  getonlinebeats():Observable <any> {
    const httpOptions = {
         headers : new HttpHeaders({
            "Content-Type":  "application/json",
            "Authorization": "trevor",
            "Access-Control-Allow-Origin" : "*"
         })
     }
    return this.http.get<any>(this.url + "/getbeats",httpOptions)
      .pipe(
        retry(3),
      );
  }
  //route to get searchdata profilepage
  getsearchdata(idspec):Observable <any> {
    const httpOptions = {
         headers : new HttpHeaders({
            "Content-Type":  "application/json",
            "Authorization": "trevor",
            "Access-Control-Allow-Origin" : "*"
         })
     }
    return this.http.post<any>(this.url + "/searchdata",idspec,httpOptions)
      .pipe(
        retry(3),
      );
  }
  //route to get searchdata profilepage
  getsearchdataoninit():Observable <any> {
    const httpOptions = {
         headers : new HttpHeaders({
            "Content-Type":  "application/json",
            "Authorization": "trevor",
            "Access-Control-Allow-Origin" : "*"
         })
     }
    return this.http.get<any>(this.url + "/fillupsearchpage",httpOptions)
      .pipe(
        retry(3),
      );
  }
  //route to get mapping data
  getmapsdata(idspec):Observable <any> {
    const httpOptions = {
         headers : new HttpHeaders({
            "Content-Type":  "application/json",
            "Authorization": "trevor",
            "Access-Control-Allow-Origin" : "*"
         })
     }
    return this.http.post<any>(this.url + "/infinitemapsretrieve",idspec,httpOptions)
      .pipe(
        retry(3),
      );
  }
  allocate(idspec):Observable <any> {
    const httpOptions = {
         headers : new HttpHeaders({
            "Content-Type":  "application/json",
            "Authorization": "trevor",
            "Access-Control-Allow-Origin" : "*"
         })
     }
    return this.http.post<any>(this.url + "/infiniteallocations",idspec,httpOptions)
      .pipe(
        retry(3),
      );
  }
  views(idspec):Observable <any> {
    const httpOptions = {
         headers : new HttpHeaders({
            "Content-Type":  "application/json",
            "Authorization": "trevor",
            "Access-Control-Allow-Origin" : "*"
         })
     }
    return this.http.post<any>(this.url + "/infiniteviews",idspec,httpOptions)
      .pipe(
        retry(3),
      );
  }
  report(idspec):Observable <any> {
    const httpOptions = {
         headers : new HttpHeaders({
            "Content-Type":  "application/json",
            "Authorization": "trevor",
            "Access-Control-Allow-Origin" : "*"
         })
     }
    return this.http.post<any>(this.url + "/infinitereporting",idspec,httpOptions)
      .pipe(
        retry(3),
      );
  }
  //sockets code goes here
  //emit a follow event
  public follow(args){
    this.socketio.emit("follow",args);
  }
  //emit unfollow request
  public unfollow(args){
    this.socketio.emit("unfollow",args);
  }
  //emit a like event
  public like(args){
    this.socketio.emit("like",args);
  }
  //emit a like event
  public save(args){
    this.socketio.emit("save",args);
  }
  //emit a like event
  public unsave(args){
    this.socketio.emit("unsave",args);
  }
  //emit a unlike event
  public unlike(args){
    this.socketio.emit("unlike",args);
  }
  //emit a message event
  public message(args){
    this.socketio.emit("message",args);
  }
  //emit a message event
  public reported(args){
    this.socketio.emit("reporting",args);
  }
  //emit a maps event
  public map(args){
    this.socketio.emit("infinitemapssave",args);
  }
}
