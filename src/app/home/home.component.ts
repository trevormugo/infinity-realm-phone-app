import { Component, AfterViewInit , ViewChild, ElementRef , OnInit ,ChangeDetectorRef} from "@angular/core";
import * as dialog from "tns-core-modules/ui/dialogs";
import { AndroidApplication, AndroidActivityBackPressedEventData } from "tns-core-modules/application";
import { Page } from "tns-core-modules/ui/page";
import { View } from "tns-core-modules/ui/core/view";
import { LoginService } from "../login.service";
import { RouterExtensions } from "nativescript-angular/router";
import { TNSFancyAlert, TNSFancyAlertButton } from "nativescript-fancyalert";
import * as app from "tns-core-modules/application";
import * as appSettings from "tns-core-modules/application-settings";
import { Adress } from "../ip.service";
import { isAndroid, isIOS } from "tns-core-modules/platform";
registerElement("Mapbox", () => require("nativescript-mapbox").MapboxView);
import { HomeService } from "../home.service";
import * as imageModule from 'nativescript-image';
import { Observable , BehaviorSubject} from "rxjs";
import { ObservableArray } from "tns-core-modules/data/observable-array";
import * as application from "application";
import { Blur } from 'nativescript-blur';
import { AudioPlayerOptions, AudioRecorderOptions, TNSPlayer, TNSRecorder } from 'nativescript-audio';
import { PanGestureEventData } from "tns-core-modules/ui/gestures";
import { SwipeGestureEventData } from "tns-core-modules/ui/gestures";
import { TouchGestureEventData } from "tns-core-modules/ui/gestures";
import { screen } from "tns-core-modules/platform"
import * as imagepicker from "nativescript-imagepicker";
import { Image } from "tns-core-modules/ui/image";
var admob = require("nativescript-admob");
import { PhotoEditor, PhotoEditorControl } from "nativescript-photo-editor";
import { ImageSource } from "tns-core-modules/image-source";
import * as frameModule from "tns-core-modules/ui/frame";
import { VideoEditor } from 'nativescript-video-editor';
const bghttp = require("nativescript-background-http");
const fs = require("file-system");
import {
  resumeEvent,
  suspendEvent,
  exitEvent,
  launchEvent,
  ApplicationEventData,
  on as applicationOn,
  run as applicationRun } from "tns-core-modules/application";
import {ActivatedRoute} from '@angular/router';
import { Router } from "@angular/router";
import { connectionType, getConnectionType, startMonitoring, stopMonitoring }from "tns-core-modules/connectivity";
import { RadListView, ListViewItemSnapMode , ListViewLoadOnDemandMode ,ListViewEventData } from "nativescript-ui-listview";
import * as Toast from 'nativescript-toast';
// somewhere at top of your component or bootstrap file
import { LogLevel, FFmpeg } from 'nativescript-ffmpeg-plugin-fixed';
import { registerElement } from "nativescript-angular/element-registry";
import { Video } from 'nativescript-videoplayer';
registerElement("VideoPlayer", () => Video);
registerElement(
  'Fab',
  () => require('@nstudio/nativescript-floatingactionbutton').Fab
);
//var vr = require('nativescript-videorecorder');
//registerElement("AdvancedVideoView", () => vr);
import { Cache } from "tns-core-modules/ui/image-cache";
import { Feedback, FeedbackType, FeedbackPosition } from "nativescript-feedback";
const permissions = require('nativescript-permissions');
import { device } from 'tns-core-modules/platform';
import { PropertyChangeData } from "data/observable";


declare var android: any; // <- important! avoids namespace issues
const WorkerScript = require("nativescript-worker-loader!../workers/file-calculations.js");
var worker;

var mypostdata;
@Component({
    selector: "ns-home",
    providers: [ Adress , HomeService , Video],
    moduleId: module.id,
    templateUrl: "./home.component.html",
    styleUrls: ["./home.component.css"],
})
export class HomePageComponent implements OnInit {
  public resultfilter:String = "accounts";
  public myprofilescoins = 0;
  public resultfilteroff:Boolean = false;
  public resultaccounts = [];
  public resultvideos = [];
  public resultaudio = [];
  public shouldiexit = 1;
  public msgesisshowing:Boolean = false;
  public tappedtwice:Boolean = false;
  public otherprofileisshowing:Boolean = false;
  public videoplayisshoeing:Boolean = false;
  public audioplayerisshowing:Boolean = false;
  public uploaderisshowing:Boolean = false;
  public myprofileisshowing:Boolean = false;
  public index:Number=0;
  public username2:String;
  public actusername:String;
  public categories3 = [
    {
      uploads:[],
      category: "All Uploads",
      pic: "InfinityRealm",
    },
    {
      uploads:[],
      category: "Audio",
      pic: "InfinityRealm",
    },
    {
      uploads:[],
      category: "Videos",
      pic: "InfinityRealm",
    },
    {
      uploads:[],
      category: "Liked",
      pic: "InfinityRealm",
    },
  ];
  public ctrlsshow:Boolean=true;
  public myemail2:String;
  public myphone2:String;
  public myfullname2:String;
  public followerscount2;
  public followingcount2;
  public postscount2;
  public myprofilepicture2;
  public username:String;
  public categories2 = [
    {
      uploads:[],
      category: "All Uploads",
      pic: "InfinityRealm",
    },
    {
      uploads:[],
      category: "Audio",
      pic: "InfinityRealm",
    },
    {
      uploads:[],
      category: "Videos",
      pic: "InfinityRealm",
    },
    {
      uploads:[],
      category: "Liked",
      pic: "InfinityRealm",
    },
    {
      uploads:[],
      category: "Saved",
      pic: "InfinityRealm",
    },
  ];
    public myemail:String;
    public myphone:String;
    public myfullname:String;
    public followerscount;
    public followingcount;
    public postscount;
    public allmyuploads = [];
    public onlymyaudiouploads = [];
    public onlymyvideouploads = [];
    public onlylikeduploads = [];
    public readthat2:String = this.myurl.getIp() + "/reader/";
    public myid = appSettings.getString("idr");
    public readthumbnail:String = this.myurl.getIp()+"/coverimage/";
    public myprofilepicture:String=this.myurl.getIp()+"/profilenoneupload/"+this.myid;
    public currentdata;
    public track:String= "Song Name";
    public query;
    public postlikes;
    public playing:Boolean=false;
    public showaudiobehind:Boolean=false;
    public mutedtwo:Boolean=true;
    public muted:Boolean=false;
    public playvid:Boolean=false;
    public tracetime;
    public intervals;
    public vidintervals;
    public myiconsfilter;
    public sliderValue1 = 0;
    public mediaplayer:TNSPlayer;
    public publicwidth;
    public thumbsrc;
    public messagetxt;
    public videosrc;
    public mycurrentsrc;
    public tabSelectedIndex:Number=0;
    public songs=[];
    public videos=[];
    public mycurrent;
    public showvideobehind = false;
    public doifollow;
    public doilike;
    public audioisplaying:Boolean=true;
    public isitlocal;
    public videoplaying:Boolean = true;
    public mysub = new BehaviorSubject(this.read());
    public profileimage:String;
    public cover:String;
    public sliderValuevid;
    public error:Boolean = false;
    public nointernet:Boolean = false;
    public readthat:String = this.myurl.getIp() + "/reader/";
    public whatwearedoing;
    private imageSource: ImageSource;
    public input;
    public pathtofile;
    public mimeseparate;
    public uploadingthumbnail;
    public imagenametoupload;
    public notme;
    public currentviewofprofileid;
    public notifications=[];
    public messages=[];
    public actmessages=[];
    public actuallchats=[];
    public mapsisshowing = false;
    public otherprofilepicture:String=this.myurl.getIp()+"/profilenoneupload/";
    public categories= [];
    public query44;
    public id = appSettings.getString("idr");
    public shouldlisten = false;
    public description;
    public name;
    public minDate: Date = new Date(1975, 0, 29);
    public maxDate: Date = new Date(4019, 4, 12);
    public mappublic;
    public daytext = "day";
    public monthtext = "month";
    public yeartext = "year";
    public addlat;
    public addlong;
    public mypoints;
    public myfollowingpoints;
    public photoEditor = new PhotoEditor();
    public time;
    public newduration;
    public nextindex;
    public artistwhosangthis;
    public argsinit;
    public showopt = false;
    public metaforplayer;
    public saves;
    public didisave;
    public durationtemplatetraced;
    public cache = new Cache();
    public nativeadvancedad;
    public potentialthumbnailimages = [];
    public countdown = 5;
    private feedback: Feedback;
    public canrecord = true;
    public isrecording = false;
    private _recorder;
    private currentholder;
    private currentbeats;
    private studiotype = "storage";
    private onlinebeats;
    private _numberOfAddedItems;
    public imcurrentlylisteningto;
    public classvideo;
    public mynewwidth = 0;
    public viewtimeout;
    public adtimeout;
    public tnsduration;
    ngOnInit() {
        this.page.actionBarHidden = true;
        mypostdata  = [
          {
            uploads:[],
            category: "Latest Releases",
            pic: "InfinityRealm",
            key: "online"
          },
          {
            uploads:[],
            category: "Top Songs",
            pic: "InfinityRealm",
            key: "online"
          },
          {
            uploads:[],
            category: "Infinite Talent",
            pic: "InfinityRealm",
            key: "online"
          },
          {
            uploads:[],
            category: "Top Albums",
            pic: "InfinityRealm",
            key: "stacked"
          },
          {
            uploads:[],
            category: "Top Playlists",
            pic: "InfinityRealm",
            key: "stacked",
          },
          {
            uploads:[],
            category: "Top Artists",
            pic: "InfinityRealm",
            key: "artists"
          },
        ];
        worker = new WorkerScript();
        this._changeDetectionRef.detectChanges();
        const video234 = this.page.getViewById("topsmallvideo");
        //handle back event
        if(isAndroid)
        {
          //var builder = new com.google.android.gms.ads.AdLoader.Builder("ca-app-pub-3940256099942544/2247696110");
          application.android.on(AndroidApplication.activityBackPressedEvent, (data: AndroidActivityBackPressedEventData) => {
            if(this.msgesisshowing === true)
            {
              data.cancel = true; // prevents default back button behavior
              this.uninitmessageplatform2();
            }
            else if(this.otherprofileisshowing === true)
            {
              data.cancel = true; // prevents default back button behavior
              this.closeothersprofpge22();
            }
            else if(this.myprofileisshowing === true)
            {
              data.cancel = true; // prevents default back button behavior
              this.closeprofpge2();
            }
            else if(this.mapsisshowing === true)
            {
              data.cancel = true; // prevents default back button behavior
              this.closemappings();
            }
            else if(this.videoplayisshoeing === true)
            {
              data.cancel = true; // prevents default back button behavior
              this.closevideoplayer();
            }
            else if(this.audioplayerisshowing === true)
            {
              data.cancel = true; // prevents default back button behavior
              this.close();
            }
            else if(this.uploaderisshowing === true)
            {
              data.cancel = true; // prevents default back button behavior
              this.hideuploader2();
            }
            else
            {
              if(this.shouldiexit === 2)
              {
                //not tapped twice
                data.cancel = false;
              }
              else
              {
                data.cancel = true;
                this.shouldiexit = this.shouldiexit + 1;
                //reset it after 1.5 seconds
                setTimeout(()=>{this.shouldiexit = 1;},1500);
                var toast = Toast.makeText("Tap again to exit." , "long");
                toast.show();
              }
            }
          });
        }
        this.page.addCss("#0{color : #ff8011;}");
        var myid = appSettings.getString("idr");
        application.android.startActivity.getWindow().setFlags(android.view.WindowManager.LayoutParams.FLAG_LAYOUT_NO_LIMITS,
                      android.view.WindowManager.LayoutParams.FLAG_LAYOUT_NO_LIMITS);
        this.profileimage = this.myurl.getIp() + "/profilenoneupload/" + myid;
        this.cover = this.myurl.getIp()+"/coverimage/";
        if(!permissions.hasPermission(android.Manifest.permission.ACCESS_NETWORK_STATE))
        {
          this.askpermission();
        }
        else
        {
          this.networkstate();
        }
        //read the videos
        this.videoread();
        //the audio is being automatically read somewhere else
        //the media player
        //i guess we wil have to initialize the player on init like we did with the website
        //the res will come from the app settings or alternatively we subscribe to the route and keep on changing it
        //what we will do is we will subscribe to the appsettings so thst every time we change the value we will do sth
        this.mediaplayer = new TNSPlayer();
        this.mediaplayer.debug = true; // set true to enable TNSPlayer console logs for debugging.
        setTimeout(()=>{
        this.mysub.subscribe((data)=>{
          console.log(data);
          clearTimeout(this.viewtimeout);
          this.mycurrentsrc = data.uploader2;
          this.currentdata = data.idr;
          this.imcurrentlylisteningto = data;
          this.manualfetchnextvideo({mimetype : data.type,from : data.from ,category : data.category , data : data.data ,idr : data.idr});
          //prepare video ad
          setTimeout(()=>{
            this.preparead()
          },1000);
          if(data.type === "video/avi" || data.type === "video/flv" || data.type === "video/mp4" || data.type === "video/mov" || data.type === "video/wmv" || data.type === "video/webm" || data.type === "video/webvtt" || data.type === "video/ogt")
          {
            //videoplayer
            this.mediaplayer.dispose();

            this.audioisplaying = false;
            clearInterval(this.intervals);
            clearInterval(this.vidintervals);
            //show the video player
            if(data.from === "library")
            {
              //from the library
              this.isitlocal = true;
              this.myiconsfilter = false;
              this.currentholder = data;
              let newobj: View = <View>this.page.getViewById("videohidden");
              newobj.animate({
                  translate:{x: 0 , y: 0},
                  duration: 200
              });
              //hide the audio player
              let oldobject: View = <View>this.page.getViewById("prevouslyhidden");
              oldobject.animate({
                  translate:{x: -this.widthforanime , y: 0},
                  duration: 200
              });
            }
            else
            {
              //from the internet
              this.isitlocal = false;
              this.myiconsfilter = true;
              if(data.uploader === myid)
              {
                this.notme = false;
              }
              else
              {
                this.notme = true;
              }
              this.fetchextradata();
              let newobj: View = <View>this.page.getViewById("videohidden");
              newobj.animate({
                  translate:{x: 0 , y: 0},
                  duration: 200
              });
              //hide the audio player
              let oldobject: View = <View>this.page.getViewById("prevouslyhidden");
              oldobject.animate({
                  translate:{x: -this.widthforanime , y: 0},
                  duration: 200
              });
            }
            this.videosrc = data.data;
            this.track = data.name;
            this.classvideo = this.page.getViewById("myvideo") as Video;
            this.playvid = true;
            /*let tempdurr = this.classvideo.getDuration() * 0.001;
            console.log("DURRRRR " + tempdurr);let minutesdurr= Math.floor(tempdurr / 60);
            let secondsdurr= Math.floor(tempdurr % 60);
            let b = secondsdurr.toString();
            if(secondsdurr < 10)
            {
              b = "0" + secondsdurr.toString();
            }
            this.durationtemplatetraced = minutesdurr + ":" + b;
            */
            this.myvideocalculations(data);
          }
          else
          {
            //audioplayer
            this.audioisplaying = true;
            this.playvid = false;
            this.videosrc = "";
            this.track = data.name;
            try {
              const vid = this.page.getViewById("myvideo") as Video;
              const vid2 = this.page.getViewById("myvideo2") as Video;
              vid.pause();
              vid2.pause();
            }
            catch(err){
              console.log("ERROR "+err);
            }
            if(data.from === "library")
            {
              //dont show the follow , like , commenticons
              //use thumbnail in device
              this.myiconsfilter = false;
              this.currentholder = data;
              //lets work on the thumbnail
              //use the one in the apps sirectory
              this.thumbsrc = "~/images/mountains_and_wind_and__infinity_sign_by_gawrifort-d5sfj0f.jpg";
              let newobj: View = <View>this.page.getViewById("prevouslyhidden");
              newobj.animate({
                  translate:{x: 0 , y: 0},
                  duration: 200
              });
              //hide the video player
              let oldobject: View = <View>this.page.getViewById("videohidden");
              oldobject.animate({
                  translate:{x: -this.widthforanime , y: 0},
                  duration: 200
              });
              this.mediaplayer
                .playFromFile({
                   audioFile: data.data, // ~ = app directory
                   loop: false,
                   completeCallback: this._trackComplete.bind(this),
                   errorCallback: this._trackError.bind(this)
                })
                .then(() => {
                   this.mediaplayer.getAudioTrackDuration().then(duration => {
                     this.tnsduration = duration;
                     console.log(`song duration:`, duration);
                   });
                });

            }
            else
            {
              this.fetchextradata();
              //from internet show the icons and the thumbnail is provided
              this.myiconsfilter = true;
              this.thumbsrc = this.cover + data.idr;
              if(data.uploader === myid)
              {
                this.notme = false;
              }
              else
              {
                this.notme = true;
              }
              let newobj: View = <View>this.page.getViewById("prevouslyhidden");
              newobj.animate({
                  translate:{x: 0 , y: 0},
                  duration: 200
              });
              //hide the video player
              let oldobject: View = <View>this.page.getViewById("videohidden");
              oldobject.animate({
                  translate:{x: -this.widthforanime , y: 0},
                  duration: 200
              });
              this.mediaplayer
                .playFromUrl({
                   audioFile: data.data, // ~ = app directory
                   loop: false,
                   completeCallback: this._trackComplete.bind(this),
                   errorCallback: this._trackError.bind(this)
                })
                .then(() => {
                   this.mediaplayer.getAudioTrackDuration().then(duration => {
                     this.tnsduration = duration;
                     console.log(`song duration:`, duration);
                   });
                });

            }
            console.log("trtrt "+this.track);
            this.playing = true;


            //trackplayer.reset();
            //trackplayer.setDataSource(data.data);
            //trackplayer.prepare();
            //trackplayer.start();
            //trackplayer.setVolume(0,0);
            clearInterval(this.intervals);
            clearInterval(this.vidintervals);
            //console.log(trackplayer.getDuration());
            //console.log(trackplayer.getCurrentPosition());
            //this.mediaplayer = trackplayer;
            this.myaudiocalculations(data);
          }
        },
        (error)=>{
        //  console.log(error);
        });
      },2000);
    }
    constructor(private _changeDetectionRef: ChangeDetectorRef , public video : Video , private router: Router , public active : ActivatedRoute , public myurl: Adress , public homeservice : HomeService , public routerextensions : RouterExtensions, public page : Page) {
      this.feedback = new Feedback();
      this._recorder = new TNSRecorder();
      this._recorder.debug = true; // set true for tns_recorder logs
      //this.imageCropper = new ImageCropper();
      applicationOn(suspendEvent, (args: ApplicationEventData) => {
          // args.android is an android activity
          if (args.android) {
              console.log("SUSPEND Activity: " + args.android);
          }
      });
      applicationOn(exitEvent, (args: ApplicationEventData) => {
          // args.android is an android activity
          if (args.android) {
              console.log("Exit Activity: " + args.android);
              //this.mediaplayer.reset();
          }
      });

    }
    //template selector
    public templateSelector(item: any, index: number, items: any) {
      if(item.key === "online") {
        return "online"
      }
      if(item.key === "stacked") {
        return "stacked";
      }
      if(item.key === "artists") {
        return "artists";
      }
      throw new Error("Unrecognized template!")
    }
    //fetchextra
    public fetchextradata(){
      var myid = appSettings.getString("idr");
      const idsummary = {
        myid : myid,
        uploaderid: this.mycurrentsrc,
        postid: this.currentdata,
      };
      this.homeservice.getextra(idsummary)
          .subscribe((data)=>{
            if(data.error === "error")
            {
              console.log("error occured");
            }
            else
            {
              console.log(data);
              this.doifollow = data.doifollow;
              this.doilike = data.doilike;
              this.postlikes = data.likes;
              this.saves = data.saves;
              this.didisave = data.didisave;
              this.artistwhosangthis = data.metadata.userName;

            }
          },(error)=>{

          });
    }
    //player controls
    public play(){
      if(!this.mediaplayer.isAudioPlaying())
      {
        //paused
        this.mediaplayer.resume();
        console.log(this.mediaplayer.isAudioPlaying());
      }
      else
      {
        //not paused
        this.mediaplayer.pause();
        console.log(this.mediaplayer.isAudioPlaying());
      }
      this.playing = this.mediaplayer.isAudioPlaying();
    }
    public manualfetchnextvideo(args){
      this.metaforplayer = {
        mimetype : args.mimetype,
        from : args.from,
        category : args.category,
        data : args.data,
        idr : args.idr
      };
    }
    public adanimate(args){
        this.showopt = true;
        this.argsinit = {
          from : args.from ,
          category : args.category ,
          data : args.data ,
          idr : args.idr
        };
        let newobj: View = <View>this.page.getViewById("advert");
        newobj.animate({
            translate:{x: 0 , y: 0},
            duration: 200
        });
        //hide the video player
        let oldobject: View = <View>this.page.getViewById("prevouslyhidden");
        oldobject.animate({
            translate:{x: -this.widthforanime , y: 0},
            duration: 200
        });
        let oldobject2: View = <View>this.page.getViewById("videohidden");
        oldobject2.animate({
            translate:{x: -this.widthforanime , y: 0},
            duration: 200
        });
      this.adtimeout = setTimeout(()=>{
        //clearInterval(interval);
        newobj.animate({
            translate:{x: -this.widthforanime , y: 0},
            duration: 200
        });

        this.showopt = false;
        this.secondphase(args);
      },5000);
    }
    public allocatepoints(){
      var myid = appSettings.getString("idr");
      const idsummary = {
        myid : myid,
        from : this.imcurrentlylisteningto.from,
        uploaderid: this.mycurrentsrc,
        postid: this.currentdata,
      };
      this.homeservice.allocate(idsummary)
          .subscribe((data)=>{
            if(data.error === "error")
            {
              console.log("error occured");
            }
            else
            {
              TNSFancyAlert.showSuccess(
                "Updated!",
                "Successfuly updated.",
                "close"
              ).then(() => {
              });
            }
          },(error)=>{
            this.feedback.show({
              title: "ERROR!",
              position: FeedbackPosition.Top, // iOS only
              type: FeedbackType.Error, // this is the default type, by the way
              message: "an error occured we could not update youre points at this time",
              duration: 3000,
              onTap: () => this.feedback.hide(),
            });
          });
    }
    public adphase(args){
      //clearTimeout(this.adtimeout);
      //setTimeout(()=>{this.secondphase(args)},5000);
      console.log(args);
      try{
        //present ad
        admob.showRewardedVideoAd({
          onRewarded: (reward) => {
            console.log("onRewarded");
            this.allocatepoints();
          },
          onRewardedVideoAdLeftApplication: () => {
            console.log("onRewardedVideoAdLeftApplication");
          },
          onRewardedVideoAdClosed: () => {
            console.log("onRewardedVideoAdClosed");
          },
          onRewardedVideoAdOpened: () => {
            console.log("onRewardedVideoAdOpened");
          },
          onRewardedVideoStarted: () => {
            console.log("onRewardedVideoStarted");
          },
          onRewardedVideoCompleted: () => {
            console.log("onRewardedVideoCompleted");
          },
        }).then(
          () => {
            console.log("RewardedVideoAd showing");
          },
          (error) => {
            console.log("admob showRewardedVideoAd error: " + error);
          }
        )
      }
      catch(error){
        console.log("ad was not prepared");
        this.secondphase(args);
      }
    }
    public preparead(){
      if(this.nointernet === false)
      {
        //prepare ad
        admob.preloadRewardedVideoAd({
          testing: true,
          iosAdPlacementId: "ca-app-pub-3940256099942544/5224354917", // add your own
          androidAdPlacementId: "ca-app-pub-3940256099942544/5224354917", // add your own
          keywords: ["music", "games"], // add keywords for ad targeting
        }).then(
          () => {
            console.log("TTTTTT = RewardedVideoAd preloaded - you can now call 'showRewardedVideoAd' whenever you're ready to do so");
          },
          (error) => {
            console.log("admob preloadRewardedVideoAd error: " + error);
          }
        )
      }
      else
      {
        //do nothing
      }
    }
    public secondphase(args){
      this.argsinit = "";
      //check if there is an internet connection or if there is an error
      if(this.error === false && this.nointernet === false)
      {
        //it might be from the library
        if(args.from === "library")
        {
          if(args.type === 'audio/mp3' || args.type === 'audio/ogg' || args.type === 'audio/wav' || args.type === 'audio/aac')
          { console.log("dfd");
            //we have the object
            var nextindex = this.songs.findIndex(item => item.songdata === this.metaforplayer.data);
            if(nextindex === this.songs.length)
            {
              nextindex = 0;
            }
            else
            {
              nextindex = nextindex + 1;
            }
            let datatoreturn = {
              "category": "library",
              "data" :this.songs[nextindex].songdata,
              "name" :this.songs[nextindex].songname,
              "type" : "audio/mp3",
              "from" : "library",
              "idr" : "library",
              "uploader" : "uploader",
              "uploader2" : "uploader2"
            };
          this.playaudio(datatoreturn);
          }
          else
          {
            console.log("yyuyioiu");
            var nextindex = this.videos.findIndex(item => item.videodata === this.metaforplayer.data);
            if(nextindex === this.videos.length)
            {
              nextindex = 0;
            }
            else
            {
              nextindex = nextindex + 1;
            }
            let datatoreturn = {
              "category": "library",
              "data" :this.videos[nextindex].videodata,
              "name" :this.videos[nextindex].videoname,
              "type" : "video/mp4",
              "from" : "library",
              "idr" : "library",
              "uploader" : "uploader",
              "uploader2" : "uploader2"
            };
            this.playaudio(datatoreturn);
          }
        }
        else
        {
          var indexofcat = this.categories.findIndex(item => item.category === this.metaforplayer.category);
          console.log(indexofcat + this.metaforplayer.category);

          var indexofupload = this.categories[indexofcat].uploads.findIndex(item => item._id === this.metaforplayer.idr);
          var length = this.categories[indexofcat].uploads.length - 1;
          if(indexofupload === length)
          {
            indexofcat = indexofcat + 1;
            indexofupload = 0;
          }
          else
          {
            indexofupload = indexofupload + 1;
          }
          console.log(indexofcat + " + " + indexofupload);
          if(this.categories[indexofcat].uploads[indexofupload].mimetype === 'audio/mp3' || this.categories[indexofcat].uploads[indexofupload].mimetype === 'audio/ogg' || this.categories[indexofcat].uploads[indexofupload].mimetype === 'audio/wav' || this.categories[indexofcat].uploads[indexofupload].mimetype === 'audio/aac')
          {
            let datatoreturn = {
              "category": this.metaforplayer.category,
              "data" :this.readthat + this.categories[indexofcat].uploads[indexofupload]._id,
              "name" :this.categories[indexofcat].uploads[indexofupload].display,
              "type" : this.categories[indexofcat].uploads[indexofupload].mimetype,
              "from" : "internet",
              "idr" : this.categories[indexofcat].uploads[indexofupload]._id,
              "uploader" : this.categories[indexofcat].uploads[indexofupload].uploader,
              "uploader2" : this.categories[indexofcat].uploads[indexofupload].uploader2
            };
            setTimeout(()=>{
              this.playurl(datatoreturn);
              let obj: View = <View>this.page.getViewById("videohidden");
              obj.animate({
                  translate:{x: -this.widthforanime , y: 0},
                  duration: 200
              });
              let newobj: View = <View>this.page.getViewById("prevouslyhidden");
              newobj.animate({
                  translate:{x: 0 , y: 0},
                  duration: 200
              });
            },1000);
          }
          else
          {
            //there is no error and there is an internet connection fetch from the server array
            //we have the object
            let datatoreturn = {
              "category": this.metaforplayer.category,
              "data" :this.readthat + this.categories[indexofcat].uploads[indexofupload]._id,
              "name" :this.categories[indexofcat].uploads[indexofupload].display,
              "type" : this.categories[indexofcat].uploads[indexofupload].mimetype,
              "from" : "internet",
              "idr" : this.categories[indexofcat].uploads[indexofupload]._id,
              "uploader" : this.categories[indexofcat].uploads[indexofupload].uploader,
              "uploader2" : this.categories[indexofcat].uploads[indexofupload].uploader2
            };
            setTimeout(()=>{
              this.playurl(datatoreturn);
              let obj: View = <View>this.page.getViewById("videohidden");
              obj.animate({
                  translate:{x: 0 , y: 0},
                  duration: 200
              });
              let newobj: View = <View>this.page.getViewById("prevouslyhidden");
              newobj.animate({
                  translate:{x: this.widthforanime , y: 0},
                  duration: 200
              });
            },1000);

          }
        }
      }
      else
      {
        if(args.type === 'audio/mp3' || args.type === 'audio/ogg' || args.type === 'audio/wav' || args.type === 'audio/aac')
        {
          //we have the object
          var nextindex = this.songs.findIndex(item => item.songdata === this.metaforplayer.data);
          if(nextindex === this.songs.length)
          {
            nextindex = 0;
          }
          else
          {
            nextindex = nextindex + 1;
          }
          let datatoreturn = {
            "category": "library",
            "data" :this.songs[nextindex].songdata,
            "name" :this.songs[nextindex].songname,
            "type" : "audio/mp3",
            "from" : "library",
            "idr" : "library",
            "uploader" : "uploader",
            "uploader2" : "uploader2"

          };
        this.playaudio(datatoreturn);
        }
        else
        {
          var nextindex = this.videos.findIndex(item => item.videodata === this.metaforplayer.data);
          if(nextindex === this.videos.length)
          {
            nextindex = 0;
          }
          else
          {
            nextindex = nextindex + 1;
          }
          let datatoreturn = {
            "category": "library",
            "data" :this.videos[nextindex].videodata,
            "name" :this.videos[nextindex].videoname,
            "type" : "video/mp4",
            "from" : "library",
            "idr" : "library",
            "uploader" : "uploader",
            "uploader2" : "uploader2"
          };
          this.playaudio(datatoreturn);
        }
      }
    }
    public next(){
      if(this.metaforplayer.from === "library")
      {
        if(this.metaforplayer.mimetype === 'audio/mp3' || this.metaforplayer.mimetype === 'audio/ogg' || this.metaforplayer.mimetype === 'audio/wav' || this.metaforplayer.mimetype === 'audio/aac')
        {
          //application.android.foregroundActivity.getWindow().clearFlags(android.view.WindowManager.LayoutParams.FLAG_FULLSCREEN);
          const View = android.view.View;
          const window = application.android.startActivity.getWindow();
          const decorView = window.getDecorView();
          decorView.setSystemUiVisibility(View.SYSTEM_UI_FLAG_LAYOUT_STABLE);
          //we have the object
          var nextindex = this.songs.findIndex(item => item.songdata === this.metaforplayer.data);
          if(nextindex === this.songs.length)
          {
            nextindex = 0;
          }
          else
          {
            nextindex = nextindex + 1;
          }
          let datatoreturn = {
            "category": "library",
            "data" :this.songs[nextindex].songdata,
            "name" :this.songs[nextindex].songname,
            "type" : "audio/mp3",
            "from" : "library",
            "idr" : "library",
            "uploader" : "uploader",
            "uploader2" : "uploader2",
          };
          setTimeout(()=>{
            this.playaudio(datatoreturn);
            let obj: View = <View>this.page.getViewById("therealvideoplayer");
            obj.animate({
                translate:{x: -this.widthforanime , y: 0},
                duration: 200
            });
            let newobj: View = <View>this.page.getViewById("animateaudio");
            newobj.animate({
                translate:{x: 0 , y: 0},
                duration: 200
            });
          },1000);
        }
        else
        {
          //we have the object
          //application.android.foregroundActivity.getWindow().addFlags(android.view.WindowManager.LayoutParams.FLAG_FULLSCREEN);
          const View = android.view.View;
          const window = application.android.startActivity.getWindow();
          const decorView = window.getDecorView();
          decorView.setSystemUiVisibility(
            View.SYSTEM_UI_FLAG_HIDE_NAVIGATION |
            View.SYSTEM_UI_FLAG_FULLSCREEN |
            View.SYSTEM_UI_FLAG_IMMERSIVE_STICKY
          );
          var nextindex = this.videos.findIndex(item => item.videodata === this.metaforplayer.data);
          if(nextindex === this.videos.length)
          {
            nextindex = 0;
          }
          else
          {
            nextindex = nextindex + 1;
          }
          let datatoreturn = {
            "category": "library",
            "data" :this.videos[nextindex].videodata,
            "name" :this.videos[nextindex].videoname,
            "type" : "video/mp4",
            "from" : "library",
            "idr" : "library",
            "uploader" : "uploader",
            "uploader2" : "uploader2",
          };
          setTimeout(()=>{
            this.playaudio(datatoreturn);
            let obj: View = <View>this.page.getViewById("therealvideoplayer");
            obj.animate({
                translate:{x: 0 , y: 0},
                duration: 200
            });
            let newobj: View = <View>this.page.getViewById("animateaudio");
            newobj.animate({
                translate:{x: this.widthforanime , y: 0},
                duration: 200
            });
          },1000);
        }
      }
      else
      {
        var indexofcat = this.categories.findIndex(item => item.category === this.metaforplayer.category);
        console.log(indexofcat + this.metaforplayer.category);

        var indexofupload = this.categories[indexofcat].uploads.findIndex(item => item._id === this.metaforplayer.idr);
        var length = this.categories[indexofcat].uploads.length - 1;
        if(indexofupload === length)
        {
          indexofcat = indexofcat + 1;
          indexofupload = 0;
        }
        else
        {
          indexofupload = indexofupload + 1;
        }
        console.log(indexofcat + " + " + indexofupload);
        if(this.categories[indexofcat].uploads[indexofupload].mimetype === 'audio/mp3' || this.categories[indexofcat].uploads[indexofupload].mimetype === 'audio/ogg' || this.categories[indexofcat].uploads[indexofupload].mimetype === 'audio/wav' || this.categories[indexofcat].uploads[indexofupload].mimetype === 'audio/aac')
        {
          //application.android.foregroundActivity.getWindow().clearFlags(android.view.WindowManager.LayoutParams.FLAG_FULLSCREEN);
          const View = android.view.View;
          const window = application.android.startActivity.getWindow();
          const decorView = window.getDecorView();
          decorView.setSystemUiVisibility(View.SYSTEM_UI_FLAG_LAYOUT_STABLE);
          //there is no error and there is an internet connection fetch from the server array
          //we have the object
          let datatoreturn = {
            "category": this.metaforplayer.category,
            "data" :this.readthat + this.categories[indexofcat].uploads[indexofupload]._id,
            "name" :this.categories[indexofcat].uploads[indexofupload].display,
            "type" : this.categories[indexofcat].uploads[indexofupload].mimetype,
            "from" : "internet",
            "idr" : this.categories[indexofcat].uploads[indexofupload]._id,
            "uploader" : this.categories[indexofcat].uploads[indexofupload].uploader,
            "uploader2" : this.categories[indexofcat].uploads[indexofupload].uploader2
          };
          setTimeout(()=>{
            this.playurl(datatoreturn);
            let obj: View = <View>this.page.getViewById("therealvideoplayer");
            obj.animate({
                translate:{x: -this.widthforanime , y: 0},
                duration: 200
            });
            let newobj: View = <View>this.page.getViewById("animateaudio");
            newobj.animate({
                translate:{x: 0 , y: 0},
                duration: 200
            });
          },1000);
        }
        else
        {
          //application.android.foregroundActivity.getWindow().addFlags(android.view.WindowManager.LayoutParams.FLAG_FULLSCREEN);
          const View = android.view.View;
          const window = application.android.startActivity.getWindow();
          const decorView = window.getDecorView();
          decorView.setSystemUiVisibility(
            View.SYSTEM_UI_FLAG_HIDE_NAVIGATION |
            View.SYSTEM_UI_FLAG_FULLSCREEN |
            View.SYSTEM_UI_FLAG_IMMERSIVE_STICKY
          );
          //there is no error and there is an internet connection fetch from the server array
          //we have the object
          let datatoreturn = {
            "category": this.metaforplayer.category,
            "data" :this.readthat + this.categories[indexofcat].uploads[indexofupload]._id,
            "name" :this.categories[indexofcat].uploads[indexofupload].display,
            "type" : this.categories[indexofcat].uploads[indexofupload].mimetype,
            "from" : "internet",
            "idr" : this.categories[indexofcat].uploads[indexofupload]._id,
            "uploader" : this.categories[indexofcat].uploads[indexofupload].uploader,
            "uploader2" : this.categories[indexofcat].uploads[indexofupload].uploader2
          };
          setTimeout(()=>{
            this.playurl(datatoreturn);
            let obj: View = <View>this.page.getViewById("therealvideoplayer");
            obj.animate({
                translate:{x: 0 , y: 0},
                duration: 200
            });
            let newobj: View = <View>this.page.getViewById("animateaudio");
            newobj.animate({
                translate:{x: this.widthforanime , y: 0},
                duration: 200
            });
          },1000);

        }
      }
    }
    public previous(){
      if(this.metaforplayer.from === "library")
      {
        if(this.metaforplayer.mimetype === 'audio/mp3' || this.metaforplayer.mimetype === 'audio/ogg' || this.metaforplayer.mimetype === 'audio/wav' || this.metaforplayer.mimetype === 'audio/aac')
        {
          //application.android.foregroundActivity.getWindow().clearFlags(android.view.WindowManager.LayoutParams.FLAG_FULLSCREEN);
          const View = android.view.View;
          const window = application.android.startActivity.getWindow();
          const decorView = window.getDecorView();
          decorView.setSystemUiVisibility(View.SYSTEM_UI_FLAG_LAYOUT_STABLE);
          //we have the object
          var nextindex = this.songs.findIndex(item => item.songdata === this.metaforplayer.data);
          if(nextindex === 0)
          {
            nextindex = this.songs.length - 1;
          }
          else
          {
            nextindex = nextindex - 1;
          }
          let datatoreturn = {
            "category": "library",
            "data" :this.songs[nextindex].songdata,
            "name" :this.songs[nextindex].songname,
            "type" : "audio/mp3",
            "from" : "library",
            "idr" : "library",
            "uploader" : "uploader",
            "uploader2" : "uploader2"
          };
          setTimeout(()=>{
            this.playaudio(datatoreturn);
            let obj: View = <View>this.page.getViewById("therealvideoplayer");
            obj.animate({
                translate:{x: -this.widthforanime , y: 0},
                duration: 200
            });
            let newobj: View = <View>this.page.getViewById("animateaudio");
            newobj.animate({
                translate:{x: 0 , y: 0},
                duration: 200
            });
          },1000);
        }
        else
        {
          //application.android.foregroundActivity.getWindow().addFlags(android.view.WindowManager.LayoutParams.FLAG_FULLSCREEN);
          const View = android.view.View;
          const window = application.android.startActivity.getWindow();
          const decorView = window.getDecorView();
          decorView.setSystemUiVisibility(
            View.SYSTEM_UI_FLAG_HIDE_NAVIGATION |
            View.SYSTEM_UI_FLAG_FULLSCREEN |
            View.SYSTEM_UI_FLAG_IMMERSIVE_STICKY
          );
          //we have the object
          var nextindex = this.videos.findIndex(item => item.videodata === this.metaforplayer.data);
          if(nextindex === 0)
          {
            nextindex =  this.videos.length - 1;
          }
          else
          {
            nextindex = nextindex - 1;
          }
          let datatoreturn = {
            "category": "library",
            "data" :this.videos[nextindex].videodata,
            "name" :this.videos[nextindex].videoname,
            "type" : "video/mp4",
            "from" : "library",
            "idr" : "library",
            "uploader" : "uploader",
            "uploader2" : "uploader2"
          };
          setTimeout(()=>{
            this.playaudio(datatoreturn);
            let obj: View = <View>this.page.getViewById("therealvideoplayer");
            obj.animate({
                translate:{x: 0 , y: 0},
                duration: 200
            });
            let newobj: View = <View>this.page.getViewById("animateaudio");
            newobj.animate({
                translate:{x: this.widthforanime , y: 0},
                duration: 200
            });
          },1000);
        }
      }
      else
      {
        var indexofcat = this.categories.findIndex(item => item.category === this.metaforplayer.category);
        var indexofupload = this.categories[indexofcat].uploads.findIndex(item => item._id === this.metaforplayer.idr);
        var length = this.categories[indexofcat].uploads.length - 1;
        console.log(indexofcat + " + " + indexofupload);
        if(indexofupload === 0)
        {
          indexofcat = indexofcat - 1;
        }
        else
        {
          indexofupload = indexofupload - 1;
        }
        if(this.categories[indexofcat].uploads[indexofupload].mimetype === 'audio/mp3' || this.categories[indexofcat].uploads[indexofupload].mimetype === 'audio/ogg' || this.categories[indexofcat].uploads[indexofupload].mimetype === 'audio/wav' || this.categories[indexofcat].uploads[indexofupload].mimetype === 'audio/aac')
        {
          //application.android.foregroundActivity.getWindow().clearFlags(android.view.WindowManager.LayoutParams.FLAG_FULLSCREEN);
          const View = android.view.View;
          const window = application.android.startActivity.getWindow();
          const decorView = window.getDecorView();
          decorView.setSystemUiVisibility(View.SYSTEM_UI_FLAG_LAYOUT_STABLE);
          //there is no error and there is an internet connection fetch from the server array
          //we have the object
          let datatoreturn = {
            "category": this.metaforplayer.category,
            "data" :this.readthat + this.categories[indexofcat].uploads[indexofupload]._id,
            "name" :this.categories[indexofcat].uploads[indexofupload].display,
            "type" : this.categories[indexofcat].uploads[indexofupload].mimetype,
            "from" : "internet",
            "idr" : this.categories[indexofcat].uploads[indexofupload]._id,
            "uploader" : this.categories[indexofcat].uploads[indexofupload].uploader,
            "uploader2" : this.categories[indexofcat].uploads[indexofupload].uploader2
          };
          setTimeout(()=>{
            this.playurl(datatoreturn);
            let obj: View = <View>this.page.getViewById("therealvideoplayer");
            obj.animate({
                translate:{x: -this.widthforanime , y: 0},
                duration: 200
            });
            let newobj: View = <View>this.page.getViewById("animateaudio");
            newobj.animate({
                translate:{x: 0 , y: 0},
                duration: 200
            });
          },1000);
        }
        else
        {
          //application.android.foregroundActivity.getWindow().addFlags(android.view.WindowManager.LayoutParams.FLAG_FULLSCREEN);
          const View = android.view.View;
          const window = application.android.startActivity.getWindow();
          const decorView = window.getDecorView();
          decorView.setSystemUiVisibility(
            View.SYSTEM_UI_FLAG_HIDE_NAVIGATION |
            View.SYSTEM_UI_FLAG_FULLSCREEN |
            View.SYSTEM_UI_FLAG_IMMERSIVE_STICKY
          );
          //there is no error and there is an internet connection fetch from the server array
          //we have the object
          var indexofcat = this.categories.findIndex(item => item.category === this.metaforplayer.category);
          var indexofupload = this.categories[indexofcat].uploads.findIndex(item => item._id === this.metaforplayer.idr);
          var length = this.categories[indexofcat].uploads.length - 1;
          console.log(indexofcat + " + " + indexofupload);
          if(indexofupload === 0)
          {
            indexofcat = indexofcat - 1;
          }
          else
          {
            indexofupload = indexofupload - 1;
          }
          let datatoreturn = {
            "category": this.metaforplayer.category,
            "data" :this.readthat + this.categories[indexofcat].uploads[indexofupload]._id,
            "name" :this.categories[indexofcat].uploads[indexofupload].display,
            "type" : this.categories[indexofcat].uploads[indexofupload].mimetype,
            "from" : "internet",
            "idr" : this.categories[indexofcat].uploads[indexofupload]._id,
            "uploader" : this.categories[indexofcat].uploads[indexofupload].uploader,
            "uploader2" : this.categories[indexofcat].uploads[indexofupload].uploader2
          };
          setTimeout(()=>{
            this.playurl(datatoreturn);
            let obj: View = <View>this.page.getViewById("therealvideoplayer");
            obj.animate({
                translate:{x: 0 , y: 0},
                duration: 200
            });
            let newobj: View = <View>this.page.getViewById("animateaudio");
            newobj.animate({
                translate:{x: this.widthforanime , y: 0},
                duration: 200
            });
          },1000);
        }
      }
    }
    public networkstate(){
      const myConnectionType = getConnectionType();
      switch (myConnectionType) {
          case connectionType.none:
              // Denotes no Internet connection.
              console.log("No connection");
              this.internet();
              break;
          case connectionType.wifi:
              // Denotes a WiFi connection.
              console.log("WiFi connection");
              this.connect();
              break;
          case connectionType.mobile:
              // Denotes a mobile connection, i.e. cellular network or WAN.
              console.log("Mobile connection");
              this.connect();
              break;
          case connectionType.ethernet:
              // Denotes a ethernet connection.
              console.log("Ethernet connection");
              this.connect();
              break;
          case connectionType.bluetooth:
              // Denotes a bluetooth connection.
              //but not connected to the internet
              console.log("Bluetooth connection");
              this.internet();
              break;
          default:
              //no other type of connection
              break;
      }
    }
    public askpermission(){
      permissions.requestPermission(android.Manifest.permission.ACCESS_NETWORK_STATE, "I need this permission to be able to tell whether you are connected to the internet or not")
      .then( () => {
        console.log("permission granted");
        this.networkstate();
      })
      .catch( () => {
        console.log("no permission");
        TNSFancyAlert.showError(
          "Error!",
          "We need you to accept this request for you to be able to access youre local music.",
          "close"
        ).then(() => {
          this.askpermission();
        });
      });
    }
    public internet(){
      this.nointernet = true;
      this.error = false;
      this.tabSelectedIndex = 5;
      //take the user to his library
    }
    public indexchnged(args){
      console.log(args.newIndex);
      this.tabSelectedIndex = args.newIndex;
    }
    public connect(){
      //now let s fetch all the uploads
      var myid = appSettings.getString("idr");
      const data = {
        id: myid,
      };
      this.homeservice.fetchinfo(data)
            .subscribe((res)=>{
              if(!res.data)
              {
                //error
                this.error = true;
                TNSFancyAlert.showError(
                  "Error!",
                  "internal server error.",
                  "close"
                ).then(() => {
                });
              }
              else
              {
                //no error
                this.error = false;
                TNSFancyAlert.showSuccess(
                  "Success!",
                  "data found.",
                  "close"
                ).then(() => {
                  mypostdata[0].uploads = res.latestreleases;
                  mypostdata[1].uploads = res.topsongs;
                  mypostdata[2].uploads = res.infinitetalent;
                  mypostdata[3].uploads = res.topalbums;
                  mypostdata[4].uploads = res.topplaylist;
                  mypostdata[5].uploads = res.topartists;
                  res.seconddata.forEach((element)=>{
                    var data2 = {
                      uploads:element.data.reverse(),
                      category: element.info.userName,
                      pic: "InfinityRealm",
                      key: "online"
                    };
                    mypostdata.push(data2);
                  });
                  this.initcategories();
                });
              }
            },
            (error)=>{
              this.error = true;
              TNSFancyAlert.showError(
                "Error!",
                "internal server error.",
                "close"
              ).then(() => {
              });
            })
      this.searchrequestinit();
      this.notificationsreq();
      this.messagesreq();
      this.fetchonlinesounds();
    }
    public initcategories(){
      this.categories;
      this._numberOfAddedItems = 0;
      for (var i = 0; i < 3 ; i++) {
          this._numberOfAddedItems++;
          this.categories.push(mypostdata[i]);
      }
    }
    public fetchonlinesounds(){
      this.homeservice.getonlinebeats()
            .subscribe((res)=>{
              this.onlinebeats = res;
            },
            (error)=>{
              this.error = true;
              TNSFancyAlert.showError(
                "Error!",
                "internal server error.",
                "close"
              ).then(() => {
              });
            })
    }
    public onRefresh(args){
      this.categories.length = 0;
      this._numberOfAddedItems = 0;
      mypostdata = [
        {
          uploads:[],
          category: "Latest Releases",
          pic: "InfinityRealm",
          key: "online"
        },
        {
          uploads:[],
          category: "Top Songs",
          pic: "InfinityRealm",
          key: "online"
        },
        {
          uploads:[],
          category: "Infinite Talent",
          pic: "InfinityRealm",
          key: "online"
        },
        {
          uploads:[],
          category: "Top Albums",
          pic: "InfinityRealm",
          key: "stacked"
        },
        {
          uploads:[],
          category: "Top Playlists",
          pic: "InfinityRealm",
          key: "stacked",
        },
        {
          uploads:[],
          category: "Top Artists",
          pic: "InfinityRealm",
          key: "artists"
        },
      ];
      this.connect();
      setTimeout(()=>{
        args.object.notifyPullToRefreshFinished();
      },1000);
    }
    public onlinesongreload(args){
      this.fetchonlinesounds();
      setTimeout(()=>{
        args.object.notifyPullToRefreshFinished();
      },1000);
    }
    public searchrequestinit(){
      this.homeservice.getsearchdataoninit()
            .subscribe((res) => {
              this.resultfilteroff = false;
              this.resultaccounts = res.data.accounts;
              this.resultaudio = res.data.onlyaudio;
              this.resultvideos = res.data.onlyvideos;
            },(error) => {
                console.log("error" + error);
            })
    }
    //click event for a song from server
    public playurl(res){
      this.mysub.next(res);
    }
    public songreload(args){
      this.songs.length=0;
      this.read();
      //clear the intervals
      setTimeout(()=>{
        args.object.notifyPullToRefreshFinished();
      },1000);
    }
    public event(args){
      console.log("trt");
    }
    public videoread(){
        if(isAndroid)
        {
          //DO ANDROID READ HERE
          const location =  android.provider.MediaStore.Video.Media.EXTERNAL_CONTENT_URI;
          const resolve = application.android.nativeApp.getContentResolver();
          var cursor = resolve.query(location,null,null,null,null);
          //THIS IS AN INTEGER
          if(cursor != null && cursor.moveToFirst())
          {
            var indextitle = cursor.getColumnIndex(android.provider.MediaStore.Video.Media.TITLE);
            var indexeddata = cursor.getColumnIndex(android.provider.MediaStore.Video.Media.DATA);
            //var indexedthumbnail=  cursor.getColumnIndex(android.provider.MediaStore.Video.Thumbnails.DATA);
            do{
              var data = cursor.getString(indexeddata);
              var title = cursor.getString(indextitle);
              //var thumbnail = cursor.getString(indexedthumbnail);
              this.videos.push({
                videoname : title,
                //videothumbnail: thumbnail,
                videodata : data
              });
              this.songs.sort((a,b)=>(a.videoname > b.videoname) ? 1 : -1 );
            }while(cursor.moveToNext());
          }
        }
        else{
          //DO IOSY THINGS HERE
        }
    }
    //function to read the audio files
    public read(){
        if(isAndroid)
        {
          //DO ANDROID READ HERE
          const location =  android.provider.MediaStore.Audio.Media.EXTERNAL_CONTENT_URI;
          const resolve = application.android.nativeApp.getContentResolver();
          var cursor = resolve.query(location,null,null,null,null);
          //THIS IS AN INTEGER
          if(cursor != null && cursor.moveToFirst())
          {
            var indextitle = cursor.getColumnIndex(android.provider.MediaStore.Audio.Media.TITLE);
            var indexartist = cursor.getColumnIndex(android.provider.MediaStore.Audio.Media.ARTIST);
            var indexeddata = cursor.getColumnIndex(android.provider.MediaStore.Audio.Media.DATA);
            do{
              var data = cursor.getString(indexeddata);
              var title = cursor.getString(indextitle);
              var artist = cursor.getString(indexartist);
              this.songs.push({
                songname : title,
                songartist : artist,
                songdata : data
              });
            }while(cursor.moveToNext());
            this.songs.sort((a,b)=>(a.songname > b.songname) ? 1 : -1 );
            return {
              "category": "library",
              "data" :this.songs[0].songdata,
              "name" :this.songs[0].songname,
              "type" : "audio/mp3",
              "from" : "library",
              "idr" : "library",
              "uploader" : "uploader",
              "uploader2" : "uploader2",
            }
          }
        }
        else
        {
          //do IOS READ HERE
        }
    }
    //click handler for a song from the cursor
    public playaudio(res){
        this.mysub.next(res);
    }
    public playaudiofromstudio(res){
        this.mysub.next(res);
        this.studiotype = res.studiotype;
    }
    public toprofilepage(){
      if(this.nointernet === true || this.error === true)
      {
        TNSFancyAlert.showError(
          "Error!",
          "No internet connection or there is a problem with our server.",
          "close"
        ).then(() => {
        });
      }
      else
      {

        const id = {
          id : this.myid,
        }
        //get summary
        this.homeservice.getsummary(id)
            .subscribe((data)=>{
              this.username = data.username;
              this.myphone = data.phonenumber;
              this.myemail = data.email;
              this.followerscount = data.followerscount;
              this.followingcount = data.followingcount;
              this.myfullname = data.fullname;
              this.postscount = data.postscount;
              this.myprofilescoins = data.coins;
              this.categories2[0].uploads = data.alluploads;
              this.categories2[1].uploads = data.onlyaudiouploads;
              this.categories2[2].uploads = data.onlyvideouploads;
              this.categories2[3].uploads = data.onlylikeduploads;
              this.categories2[4].uploads = data.onlysaveduploads;
            },(error)=>{
                TNSFancyAlert.showError(
                  "Error!",
                  "Internal server error we will fix it",
                  "close"
                ).then(() => {

                });
            });
        let profilehidden: View = <View>this.page.getViewById("myprofilepage22");
        //take down
        profilehidden.animate({
          translate:{x: 0 , y: 0 },
          duration: 200
        });
        this.myprofileisshowing = true;
      }
    }
    public closeprofpge2(){
      this.categories2[0].uploads.length = 0;
      this.categories2[1].uploads.length = 0;
      this.categories2[2].uploads.length = 0;
      this.categories2[3].uploads.length = 0;
      this.categories2[4].uploads.length = 0;
      let profilehidden: View = <View>this.page.getViewById("myprofilepage22");
      profilehidden.animate({
        translate:{x: 0 , y : -this.heightforanime},
        duration: 200
      });
      this.myprofileisshowing = false;
    }
    public closeprofpge(args:SwipeGestureEventData){
      this.categories2[0].uploads.length = 0;
      this.categories2[1].uploads.length = 0;
      this.categories2[2].uploads.length = 0;
      this.categories2[3].uploads.length = 0;
      this.categories2[4].uploads.length = 0;
      let profilehidden: View = <View>this.page.getViewById("myprofilepage22");
      if(args.direction === 8)
      {
        //swiping down
        profilehidden.animate({
          translate:{x: 0 , y : -this.heightforanime},
          duration: 200
        });
        this.myprofileisshowing = false;
      }
    }
    //video player
    public togglevidplay(){
      const vid = this.page.getViewById("myvideo") as Video;
      const vid2 = this.page.getViewById("myvideo2") as Video;
      if(this.playvid === true)
      {
        //pause it
        this.playvid = false;
        vid.pause();
        vid2.pause();
      }
      else
      {
        //play it
        this.playvid = true;
        vid.play();
        vid2.play();
      }
    }
    //animations
    public widthforanime = screen.mainScreen.widthDIPs;
    public heightforanime = -screen.mainScreen.heightDIPs;
    //animations to close the audio player through swipe gestures
    public animate(args :SwipeGestureEventData){
      console.log(args.direction);
      let audiohidden: View = <View>this.page.getViewById("animateaudio");
    //  if(args.direction === 8){
        //swipe down show comments
    //  }
    //  else if(args.direction === 4)
    //  {
        //swiping up show profile page
    //  }
      if(args.direction === 1)
      {
        //swiping right
        audiohidden.animate({
          translate:{x: this.widthforanime , y : 0},
          duration: 200
        });
      }
      this.audioplayerisshowing = false;
    }
    //animations to toggle the header
    public animateview(args :SwipeGestureEventData){
      //this will be changing according to the index
      console.log(args.direction);
      if(args.direction === 2){
        if(this.tabSelectedIndex === 3 || this.tabSelectedIndex === 4)
        {
          //do not animae anything
        }
        else
        {
          let currentobj: View = <View>args.object;
          currentobj.animate({
            translate:{x: -this.widthforanime , y: 0},
            duration: 200
          });
          //animate the new view
          let newobj: View = <View>this.page.getViewById(`maintop${this.tabSelectedIndex}`);
          newobj.animate({
            translate:{x: 0 , y: 0},
            duration: 200
          });
        }
      }
    }
    public animatehiddenview(args :SwipeGestureEventData){
      //all will reurn this one
      console.log(args.direction);
      if(args.direction === 1){
        let currentobj: View = <View>args.object;
        currentobj.animate({
          translate:{x: this.widthforanime , y: 0},
          duration: 200
        });
        //animate the new view
        if(this.audioisplaying === true){
          let newobj: View = <View>this.page.getViewById("prevouslyhidden");
          newobj.animate({
              translate:{x: 0 , y: 0},
              duration: 200
          });
         }
         else{
           let newobj: View = <View>this.page.getViewById("videohidden");
           newobj.animate({
               translate:{x: 0 , y: 0},
               duration: 200
           });
         }
      }
    }
    //animations to open and close the audio player
    public showtheplayer(){
      //animate
      let audiohidden: View = <View>this.page.getViewById("animateaudio");
      audiohidden.animate({
        translate:{x: 0 , y: 0},
        duration: 400
      });
      this.audioplayerisshowing = true;
    }
    public close(){
      let audiohidden: View = <View>this.page.getViewById("animateaudio");
      audiohidden.animate({
        translate:{x: this.widthforanime , y: 0 },
        duration: 200
      });
      this.audioplayerisshowing = false;
    }
    //animations to open close the videoplayer
    public showvideoplayer(){
      //application.android.foregroundActivity.getWindow().addFlags(android.view.WindowManager.LayoutParams.FLAG_FULLSCREEN);
      const View = android.view.View;
      const window = application.android.startActivity.getWindow();
      const decorView = window.getDecorView();
      decorView.setSystemUiVisibility(
        View.SYSTEM_UI_FLAG_HIDE_NAVIGATION |
        View.SYSTEM_UI_FLAG_FULLSCREEN |
        View.SYSTEM_UI_FLAG_IMMERSIVE_STICKY
      );
      let audiohidden: View = <View>this.page.getViewById("therealvideoplayer");
      audiohidden.animate({
        translate:{x: 0 , y: 0},
        duration: 400
      });
      this.muted = true;
      this.mutedtwo = false;
      this.videoplayisshoeing=true;
      setTimeout(()=>{
        this.ctrlsshow = false;
      },5000);
    }
    public adjust(){
      try{
        const View = android.view.View;
        const window = application.android.startActivity.getWindow();
        const decorView = window.getDecorView();
        decorView.setSystemUiVisibility(
          View.SYSTEM_UI_FLAG_HIDE_NAVIGATION |
          View.SYSTEM_UI_FLAG_FULLSCREEN |
          View.SYSTEM_UI_FLAG_IMMERSIVE_STICKY
        );
        this.showctrls();
      }
      catch(error){
        console.log(error);
      }
    }
    public closevideoplayer(){
      //application.android.foregroundActivity.getWindow().clearFlags(android.view.WindowManager.LayoutParams.FLAG_FULLSCREEN);
      const View = android.view.View;
      const window = application.android.startActivity.getWindow();
      const decorView = window.getDecorView();
      decorView.setSystemUiVisibility(View.SYSTEM_UI_FLAG_LAYOUT_STABLE);
      let audiohidden: View = <View>this.page.getViewById("therealvideoplayer");
      audiohidden.animate({
        translate:{x: -this.widthforanime , y: 0},
        duration: 400
      });
      this.muted = false;
      this.mutedtwo = true;
      this.videoplayisshoeing = false;
    }
    //animations to close the video player through gesture
    public closethroughgesture(args :SwipeGestureEventData){
      //application.android.foregroundActivity.getWindow().clearFlags(android.view.WindowManager.LayoutParams.FLAG_FULLSCREEN);
      const View = android.view.View;
      const window = application.android.startActivity.getWindow();
      const decorView = window.getDecorView();
      decorView.setSystemUiVisibility(View.SYSTEM_UI_FLAG_LAYOUT_STABLE);
      let videohidden: View = <View>this.page.getViewById("therealvideoplayer");
      this.muted = false;
      this.mutedtwo = true;
      if(args.direction === 2)
      {
        //swiping left
        videohidden.animate({
          translate:{x: -this.widthforanime , y : 0},
          duration: 200
        });
      }
      this.videoplayisshoeing = false;
    }
    //maps function
    public mapsnav(){
      if(this.nointernet === true || this.error === true)
      {
        TNSFancyAlert.showError(
          "Error!",
          "No internet connection or there is a problem with our server.",
          "close"
        ).then(() => {
        });
      }
      else
      {
        let newobj: View = <View>this.page.getViewById("innermappings");
        newobj.animate({
          translate:{x: 0 , y: 0},
          duration: 200
        });
        this.mapsisshowing = true;
      }
    }
    public closemappings(){
      let newobj: View = <View>this.page.getViewById("innermappings");
      newobj.animate({
          translate:{x: -this.widthforanime , y: 0},
          duration: 200
      });
      this.mapsisshowing = false;
    }
    //animations
    //upload functions
    public showuploadfunc(args){
      this.playing = false;
      let uploader: View = <View>this.page.getViewById("hiddenuploader");
      uploader.animate({
        translate:{x: 0 , y : 0},
        duration: 400
      });
      this.uploaderisshowing = true;
      //this.mediaplayer.pause();
      var datatoplay = {
        "category": "library",
        "data" : args.data,
        "name" : args.name,
        "type" : args.type,
        "from" : "library",
        "idr" : "library",
        "uploader" : "uploader"
      };
      this.playaudio(datatoplay);
      this.uploadingthumbnail = "";
      this.input = "";
      this.pathtofile = args.data;
      this.whatwearedoing = args.name;
      this.mimeseparate = args.type;
      if(this.mimeseparate === "video/avi" || this.mimeseparate === "video/flv" || this.mimeseparate === "video/mp4" || this.mimeseparate === "video/mov" || this.mimeseparate === "video/wmv" || this.mimeseparate === "video/webm" || this.mimeseparate === "video/webvtt" || this.mimeseparate === "video/ogt")
      {
        //it is a video auto generte a thumbnail
        //some thumbnail logic goes here
        this.videoedit();
        var toast = Toast.makeText("tap the box to change the thumbnail  , longpress the box to crop the thumbnail." , "long");
        toast.show();
      }
      else
      {
        //audio
        this.readimages();
        var toast2 = Toast.makeText("tap the box to generate a thumbnail.");
        toast2.show();
        this.feedback.show({
          title: "WARNING!",
          position: FeedbackPosition.Top, // iOS only
          type: FeedbackType.Warning, // this is the default type, by the way
          message: "Sound editing for audio files is unavailable",
          duration: 3000,
          onTap: () => this.feedback.hide(),
        });
      }
    }
    //edit video
    public videoedit(){
      var milliseconds = (new Date).getTime();
      //lets get the file duration
      var durr;
      VideoEditor.getVideoInfo(this.pathtofile)
        .then((info) => {
          console.log(info);console.log(info.duration);
          durr = info.duration;
        })
        .catch((error) => {
          console.log(error);
        })
      var randomnum =  Math.floor(Math.random() * durr);
      VideoEditor.createThumbnail(
        this.pathtofile, // the path to the video on the device
        'InfinityRealm-'+milliseconds, // the file name for the JPEG image
        randomnum, // optional, location in the video to create the thumbnail (in seconds)
        500, // optional, width of the thumbnail
        500, // optional, height of the thumbnail
        100 // optional, quality of the thumbnail (between 1 and 100)
      ).then((file) => {
        console.log(file);
        this.uploadingthumbnail = file;
      }).catch((error) => {
        console.log(error);
      });
    }
    //hide uploader
    public hideuploader(args :SwipeGestureEventData){
      let uploader: View = <View>this.page.getViewById("hiddenuploader");
      this.uploaderisshowing = false;
      if(this.isrecording === true)
      {
        this.stoprecording();
        TNSFancyAlert.showInfo(
          "Stopped recording!",
          "recording was stopped.",
          "close"
        ).then(() => {
        });
      }
      if(args.direction === 8){
        //take down
        uploader.animate({
          translate:{x: 0 , y: -this.heightforanime },
          duration: 200
        });
      }
      else if(args.direction === 4)
      {
        //swiping up
        uploader.animate({
          translate:{x: 0 , y: this.heightforanime },
          duration: 200
        });
      }
      else if(args.direction === 1)
      {
        //swiping right
        uploader.animate({
          translate:{x: this.widthforanime , y : 0},
          duration: 200
        });
      }
      else
      {
        //swiping left
        uploader.animate({
          translate:{x: -this.widthforanime , y : 0},
          duration: 200
        });
      }
    }
    public hideuploader2(){
      let uploader: View = <View>this.page.getViewById("hiddenuploader");
      this.uploaderisshowing = false;
      if(this.isrecording === true)
      {
        this.stoprecording();
        TNSFancyAlert.showInfo(
          "Stopped recording!",
          "recording was stopped.",
          "close"
        ).then(() => {
        });
      }
      uploader.animate({
        translate:{x: 0 , y: -this.heightforanime },
        duration: 200
      });
    }
    //function to crop images
    public cropmyimage(arg){
      this.photoEditor.editPhoto({
        imageSource: arg,
        hiddenControls: [
          //PhotoEditorControl.Save,
          //PhotoEditorControl.Crop,
        ],
      }).then((newImage: ImageSource) => {
          // Here you can save newImage, send it to your backend or simply display it in your app
          //resultImage.imageSource = newImage;
          var milliseconds = (new Date).getTime();
          var folder = fs.knownFolders.documents().path;
          var filename ="InfinityRealm" + milliseconds + ".png";
          var path = fs.path.join(folder , filename);
          var saved = newImage.saveToFile(path , "png");
          //save to global variabes
          this.uploadingthumbnail = path;
          this.imagenametoupload =  path.substr(path.lastIndexOf("/") + 1);
        //  this.myimagetesing = path;
          console.log("my path " + path);
          if (saved) {
          //  this.myimagetesing = path;
            console.log(path);
          }
      }).catch((e) => {
        console.error(e);
      });
      /*
      this.imageCropper
          .show(arg, { width : 500 ,height: 500 , lockSquare: true })
          .then(args => {
            //instead of doing this let us save it in a file;
            //all the images will be saved in a file for the meanwhile we can delete them if we wanted
            //but let us just hold off on that.
            var milliseconds = (new Date).getTime();
            var folder = fs.knownFolders.documents().path;
            var filename ="InfinityRealm" + milliseconds + ".png";
            var path = fs.path.join(folder , filename);
            var saved = args.image.saveToFile(path , "png");
            //save to global variabes
            this.uploadingthumbnail = path;
            this.imagenametoupload =  path.substr(path.lastIndexOf("/") + 1);
          //  this.myimagetesing = path;
            console.log("my path " + path);
            if (saved) {
            //  this.myimagetesing = path;
              console.log(path);
            }
          })
          .catch(function(e) {
              console.log(e);
          });*/
    }
    public gallery(args){
      if(this.mimeseparate === "video/avi" || this.mimeseparate === "video/flv" || this.mimeseparate === "video/mp4" || this.mimeseparate === "video/mov" || this.mimeseparate === "video/wmv" || this.mimeseparate === "video/webm" || this.mimeseparate === "video/webvtt" || this.mimeseparate === "video/ogt")
      {
        //generate another thumbnail if video
        this.videoedit();
      }
      else
      {
        //open gallery if audio
        let context = imagepicker.create({
            mode: "single" // use "multiple" for multiple selection
        });
        context.authorize().then(() => {
            return context.present();
          }).then((selection) => {
                selection.forEach((selected) => {
                  selected.getImageAsync(source => {
                  const selectedImgSource = new ImageSource(source);
                  this.cropmyimage(selectedImgSource);
                  });
                });
          }).catch((e) => {
            // process error
            console.log(e);
          });
        }
    }
    //public press upload
    public upload(){
      //first make sure the thumbnail is present
      //if present upload the thumbnail first and then the file
      if(!this.uploadingthumbnail)
      {
          //error no image was chosen
          TNSFancyAlert.showError(
            "Error!",
            "Thumbnail is required for upload.",
            "close"
          ).then(() => {
          });
      }
      else if(!this.input)
      {
          TNSFancyAlert.showError(
            "Error!",
            "Filename is required for upload.",
            "close"
          ).then(() => {
          });
      }
      else
      {
          this.audiovideofiles();
      }

    }
    //upload audio/video files first
    public audiovideofiles(){
      //before we start uploading let us check that the filename is unique
      /*IMPORTANT*/
      let session = bghttp.session("audiofile-upload");
      let params = appSettings.getString("idr");
      let request = {
          url: this.myurl.getIp()+"/fileuploadfiles/"+params+"/"+this.input,
          method: "POST",
          description: "Uploading "
      };
      let params2 = [{
        "name" : "file",
        "filename" : this.pathtofile,
        "mimeType": this.mimeseparate
      }];
      let task = session.multipartUpload(params2, request);
      task.on("error",(event)=>{
        TNSFancyAlert.showError(
          "Error!",
          "An error occured",
          "close"
        ).then(() => {
        });
      });
      task.on("progress",(event)=>{});
      task.on("complete",(event)=>{
        TNSFancyAlert.showSuccess(
          "Success",
          "audio file uploaded now uploading thumbnail",
          "close"
        ).then(() => {
        });
      });
      task.on("responded", (event)=>{
        console.log(event);
        if(event.error){
          TNSFancyAlert.showError(
            "Error!",
            "We sense foul play or there is an issue with the server.",
            "close"
          ).then(() => {
          });
        }
        else{
          console.log(event.data);
          this.thumbnailupload(event.data);
        }
      });
    }
    //then upload the image file
    public thumbnailupload(route){
      let session = bghttp.session("thumbfile-upload");
      let request = {
          url: this.myurl.getIp()+"/newcover/"+route,
          method: "POST",
          description: "Uploading "
      };
      let params2 = [{
        "name": "file" ,
        "filename": this.uploadingthumbnail,
        "mimeType": "image/png"
       }];
      let task = session.multipartUpload(params2, request);
      task.on("error",(event)=>{
        TNSFancyAlert.showError(
          "Error!",
          "An error occured the thumbnail was not uploaded ",
          "close"
        ).then(() => {
          let uploader: View = <View>this.page.getViewById("hiddenuploader");
          uploader.animate({
            translate:{x: 0 , y : this.heightforanime},
            duration: 200
          });
        });
        console.log(event);
      });
      task.on("progress",(event)=>{});
      task.on("complete",(event)=>{
        TNSFancyAlert.showSuccess(
          "Success!",
          "your file has been successfully downloaded",
          "close"
        ).then(() => {
          let uploader: View = <View>this.page.getViewById("hiddenuploader");
          uploader.animate({
            translate:{x: 0 , y : this.heightforanime},
            duration: 200
          });
        });
      });
    }
    //SHUFFLE CONTROL
    public hidemyshuffle = false;
    public shuffleme(){
      this.hidemyshuffle = !this.hidemyshuffle;
    }
    //SEARCH PAGE CODE
    public searchrequest(){
      if(!this.query)
      {
        var toast = Toast.makeText("the search box is empty." , "long");
        toast.show();
        this.searchrequestinit();
      }
      else
      {
      //logic for search request goes here
      var myid = appSettings.getString("idr");
      const data = {
        id: myid,
        searchquery: this.query,
      };
      this.homeservice.getsearchdata(data)
            .subscribe((res) => {
              this.resultfilteroff = true;
              this.resultaccounts = res.accounts;
              this.resultaudio = res.onlyaudiouploads;
              this.resultvideos = res.onlyvideouploads;
              if(this.resultaccounts.length !== 0)
              {
                this.resultfilter= "accounts";
              }
              else if(this.resultaudio.length !== 0)
              {
                this.resultfilter= "audio";
              }
              else if(this.resultvideos.length !== 0)
              {
                this.resultfilter= "videos";
              }
              else
              {
                //do nothing
              }
            },(error) => {
                console.log("error" + error);
            })
        }
    }
    //follow function
    public followfunction(){
        this.doifollow = true;
        var myid = appSettings.getString("idr");
        const data = {
          ifollow: myid,
          thisperson: this.mycurrentsrc
        };
        this.homeservice.follow(data);
    }
    public unfollowfunction(){
      //unfollow function
      this.doifollow = false;
      var myid = appSettings.getString("idr");
      const data = {
        ifollow: myid,
        thisperson: this.mycurrentsrc
      };
      this.homeservice.unfollow(data);
    }
    public unsavefunction(){
      this.didisave = false;
      var myid = appSettings.getString("idr");
      const data = {
        isave: myid,
        thispost: this.currentdata,
        ofthisperson: this.mycurrentsrc
      };
      this.homeservice.unsave(data);
      this.saves = this.saves - 1;
    }
    //save function
    public savefunction(){
      this.didisave = true;
      var myid = appSettings.getString("idr");
      const data = {
        isave: myid,
        thispost: this.currentdata,
        ofthisperson: this.mycurrentsrc
      };
      this.homeservice.save(data);
      this.saves = this.saves + 1;
    }
    //like function
    public likefunction(){
      this.doilike = true;
      var myid = appSettings.getString("idr");
      const data = {
        ilike: myid,
        thispost: this.currentdata
      };
      this.homeservice.like(data);
      this.postlikes = this.postlikes + 1;
    }
    //unlike function
    public unlike(){
      this.doilike = false;
      var myid = appSettings.getString("idr");
      const data = {
        ilike: myid,
        thispost: this.currentdata
      };
      this.homeservice.unlike(data);
      this.postlikes = this.postlikes - 1;
    }
    //play from profile function
    public playfromprof(args){
      this.mysub.next(args);
    }
    //messages page
    public messagesreq(){
      var myid = appSettings.getString("idr");
      const data = {
        id: myid,
      };
      this.homeservice.getmessages(data)
            .subscribe((res) => {
              if(res.res === "no messages")
              {
                //no notifications
                console.log("nothing");
              }
              else
              {
                //some notifications
                console.log(res);
                this.messages = res;
              }
            },(error) => {
                console.log("error" + error);
            })
    }
    //notifications page
    public notificationsreq(){
      var myid = appSettings.getString("idr");
      const data = {
        id: myid,
      };
      this.homeservice.getnotifications(data)
            .subscribe((res) => {
              if(res.res === "no notifications")
              {
                //no notifications
                console.log("nothing");
              }
              else
              {
                //some notifications
                console.log(res);
                this.homeservice.notificationsfromservice = res;
                this.notifications = this.homeservice.notificationsfromservice;
              }
            },(error) => {
                console.log("error" + error);
            })
    }
    public messagereload(args){
      this.messages.length = 0;
      this.messagesreq();
      setTimeout(()=>{
        args.object.notifyPullToRefreshFinished();
      },4000);
    }
    public notificationreload(args){
      this.notifications.length = 0;
      this.notificationsreq();
      setTimeout(()=>{
        args.object.notifyPullToRefreshFinished();
      },4000);
    }
    //follow and unfollow others from none uploads
    public noneuploadfollow(args){
      var myid = appSettings.getString("idr");
      const data = {
        ifollow: myid,
        thisperson: args
      };
      //reload the page
      this.notificationsreq();
      this.homeservice.follow(data);
    }
    public noneuploadunfollow(args){
      var myid = appSettings.getString("idr");
      const data = {
        ifollow: myid,
        thisperson: args
      };
      //reload the page
      this.notificationsreq();
      this.homeservice.unfollow(data);
    }
    //open and see other users profile pics
    public gotouserprofile(args){
      if(this.nointernet === true || this.error === true)
      {
        TNSFancyAlert.showError(
          "Error!",
          "No internet connection or there is a problem with our server.",
          "close"
        ).then(() => {
        });
      }
      else
      {
        //there is Internet
        this.otherprofileisshowing = true;
        this.myprofilepicture2 = this.myurl.getIp()+"/profilenoneupload/"+args;
        this.currentviewofprofileid = args;
        const id = {
          id : args,
        }
        //get following
        this.homeservice.getsummary(id)
            .subscribe((data)=>{
              this.username2 = data.username;
              this.myphone2 = data.phonenumber;
              this.myemail2 = data.email;
              this.followerscount2 = data.followerscount;
              this.followingcount2 = data.followingcount;
              this.myfullname2 = data.fullname;
              this.postscount2 = data.postscount;
              this.categories3[0].uploads = data.alluploads;
              this.categories3[1].uploads = data.onlyaudiouploads;
              this.categories3[2].uploads = data.onlyvideouploads;
              this.categories3[3].uploads = data.onlylikeduploads;
            },(error)=>{
                TNSFancyAlert.showError(
                  "Error!",
                  "Internal server error we will fix it",
                  "close"
                ).then(() => {

                });
            });
        let profilehidden: View = <View>this.page.getViewById("otherprofilepage33");
        //take down
        profilehidden.animate({
          translate:{x: 0 , y: 0 },
          duration: 200
        });
      }
    }
    //close the profile page
    public closeothersprofpge(args :SwipeGestureEventData){
      this.otherprofileisshowing = false;
      this.username2 = "";
      this.myfullname2 = "";
      this.myemail2 = "";
      this.myphone2 = "";
      this.postscount2 = "";
      this.followerscount2 = "";
      this.followingcount2 = "";
      this.categories3[0].uploads.length = 0;
      this.categories3[1].uploads.length = 0;
      this.categories3[2].uploads.length = 0;
      this.categories3[3].uploads.length = 0;
      this.currentviewofprofileid = "";
      let profilehidden: View = <View>this.page.getViewById("otherprofilepage33");
      if(args.direction === 8)
      {
        //swiping right
        profilehidden.animate({
          translate:{x: 0 , y : -this.heightforanime},
          duration: 200
        });
      }
    }
    public closeothersprofpge22(){
      this.otherprofileisshowing = false;
      this.username2 = "";
      this.myfullname2 = "";
      this.myemail2 = "";
      this.myphone2 = "";
      this.postscount2 = "";
      this.followerscount2 = "";
      this.followingcount2 = "";
      this.categories3[0].uploads.length = 0;
      this.categories3[1].uploads.length = 0;
      this.categories3[2].uploads.length = 0;
      this.categories3[3].uploads.length = 0;
      this.currentviewofprofileid = "";
      let profilehidden: View = <View>this.page.getViewById("otherprofilepage33");
      profilehidden.animate({
        translate:{x: 0 , y : -this.heightforanime},
        duration: 200
      });
    }
    public initmessageplatform(){
      var myid = appSettings.getString("idr");
      this.msgesisshowing = true;
      const data = {
        myid: myid,
        otherid: this.currentviewofprofileid,
      };
      var data2 = {
        id: this.currentviewofprofileid,
      }
      this.homeservice.getactmessages(data)
            .subscribe((res) => {
              if(res.res === "no actmessages")
              {
                //no actmessages
                console.log("nothing");
              }
              else
              {
                //some actmessages
                //fetch some metadata
                this.homeservice.getmetadata(data2)
                    .subscribe((res2) => {
                      console.log(res2);
                      this.actusername = res2.data.userName;
                      this.homeservice.messagesfromservice = res;
                      this.actmessages = this.homeservice.messagesfromservice;
                      //doesnt work
                      let listView: RadListView = <RadListView>(this.page.getViewById("listView333"));
                      listView.scrollToIndex(this.homeservice.messagesfromservice.length - 1, false);
                    },(error) => {
                      console.log("error" + error);
                    })
              }
            },(error) => {
                console.log("error" + error);
            })
      let profilehidden: View = <View>this.page.getViewById("otherprofilepage33");
      let messagehidden: View = <View>this.page.getViewById("messagehidden");
      profilehidden.animate({
        translate:{x: 0 , y : this.heightforanime},
        duration: 200
      });
      messagehidden.animate({
        translate:{x: 0 , y : 0},
        duration: 200
      });
    }
    public shortcuttoinitmsgplatform(args){
      //short cut to init msg platform
      this.currentviewofprofileid = args;
      this.initmessageplatform();
    }
    //by tapping
    public uninitmessageplatform2(){
      this.msgesisshowing = false;
      this.homeservice.messagesfromservice.length = 0;
      let profilehidden: View = <View>this.page.getViewById("otherprofilepage33");
      let messagehidden: View = <View>this.page.getViewById("messagehidden");
      //take down
      /*profilehidden.animate({
        translate:{x: 0 , y : 0},
        duration: 200
      });*/
      messagehidden.animate({
        translate:{x: 0 , y : -this.heightforanime},
        duration: 200
      });
    }
    //socket to message
    public messagesocket(){console.log("tapping");
      var myid = appSettings.getString("idr");
      const data = {
        senderid: myid,
        recipientid: this.currentviewofprofileid,
        message: this.messagetxt,
        messagetype: "text",
      };
      this.homeservice.message(data);
    }
    public showctrls(){
      this.ctrlsshow = true;
      setTimeout(()=>{
        this.ctrlsshow = false;
      },5000);
    }
    //MAPS
    public onMapReady(args){
      console.log("map is ready");
      this.mappublic = args.map;console.log(args.map);
      this.addmarkers();
      applicationOn(suspendEvent, (args: ApplicationEventData) => {
          //this.mappublic.destroy();
          console.log("hidden");
      });
      applicationOn(resumeEvent, (args: ApplicationEventData) => {
        //this.mappublic.unhide();
        console.log("unhidden");
      });
    }
    public addmarkers(){
      var myid = appSettings.getString("idr");
      const data = {
        id: myid,
      };
      this.homeservice.getmapsdata(data)
            .subscribe((res) => {
              //lets find out the number of points that belong to me
              this.mypoints = res.me.length;
              //the number of points that belong to the people i follow
              this.myfollowingpoints = res.following.length;
              //add the markers that belong to me
              res.me.forEach((element)=>{
                setTimeout(()=>{
                  this.mappublic.addMarkers([
                  {
                    lat: element.latitude,
                    lng: element.longitude,
                    title: element.nameofevent,
                    subtitle: element.date,
                    selected: true, // makes the callout show immediately when the marker is added (note: only 1 marker can be selected at a time)
                    onTap: function(marker){
                      console.log(marker);
                    }
                  }
                ]);
              },4000);
              });
              //add the markers that belong to my following
              res.following.forEach((element)=>{
                setTimeout(()=>{
                  this.mappublic.addMarkers([
                  {
                    lat: element.latitude,
                    lng: element.longitude,
                    title: element.nameofevent,
                    subtitle: element.date,
                    selected: true, // makes the callout show immediately when the marker is added (note: only 1 marker can be selected at a time)
                    onTap: function(marker){
                      console.log(marker);
                    }
                  }
                ]);
              },4000);
              });
            },(error) => {
                console.log("error" + error);
            })
    }
    public addlocation(){
      //logic to add location
      this.shouldlisten = true;
      console.log("tapping");
      //first we will remove all maerkers
      this.mappublic.removeMarkers();
      var toast = Toast.makeText("tap on the map to add a marker");
      toast.show();
      this.mappublic.setOnMapClickListener((point)=>{
        if(this.shouldlisten === false)
        {
          //do nothing
        }
        else
        {
          this.addlat = point.lat;
          this.addlong = point.lng;
          var toast = Toast.makeText("latitude:"+point.lat+" longitude:"+point.lng);
          toast.show();
          //show the upload box then add the markers back to the map
          let placemarkerhidden: View = <View>this.page.getViewById("placeamarker");
          placemarkerhidden.animate({
            translate:{x: 0 , y: 0 },
            duration: 200
          });
        }
      });
    }
    public onDayChanged(args) {
      this.daytext = args.value;
    }
    public onMonthChanged(args) {
      this.monthtext = args.value;
    }
    public onYearChanged(args) {
      this.yeartext = args.value;
    }
    public hidemarkuploader(args : SwipeGestureEventData){
      let placemarkerhidden: View = <View>this.page.getViewById("placeamarker");
      if(args.direction === 8){
        //take down
        placemarkerhidden.animate({
          translate:{x: 0 , y: -this.heightforanime },
          duration: 200
        });
        var toast = Toast.makeText("cancelling...");
        toast.show();
        this.daytext = "day";
        this.monthtext = "month";
        this.yeartext = "year";
        this.name = "";
        this.description = "";
        //return all markers
        this.addmarkers();
        this.shouldlisten = false;
      }
    }
    public tappedhide(){
      let placemarkerhidden: View = <View>this.page.getViewById("placeamarker");
      placemarkerhidden.animate({
        translate:{x: 0 , y: -this.heightforanime },
        duration: 200
      });
      var toast = Toast.makeText("cancelling...");
      toast.show();
      this.daytext = "day";
      this.monthtext = "month";
      this.yeartext = "year";
      this.name = "";
      this.description = "";
      //return all markers
      this.addmarkers();
      this.shouldlisten = false;
    }
    public placemark(){
      console.log("placing marker");
      if(!this.name || this.daytext === "day" || this.monthtext === "month" || this.yeartext === "year")
      {
        //fill in all required fields
        var toast = Toast.makeText("please fill in all required fields");
        toast.show();
      }
      else
      {
        this.shouldlisten = false;
        console.log("send to database");
        //return all markers
        setTimeout(()=>{this.addmarkers()},4000);
        //logic to add marker goes here.using sockets
        var data = {
          pointowner: this.id,
          longitude: this.addlong,
          latitude: this.addlat,
          date: this.daytext+"/"+this.monthtext+"/"+this.yeartext,
          descriptionofevent: this.description,
          nameofevent: this.name,
        };
        this.homeservice.map(data);
        var toast = Toast.makeText("markers have been updated");
        toast.show();
        //close the window
        this.daytext = "";
        this.monthtext = "";
        this.yeartext = "";
        this.name = "";
        this.description = "";
        let placemarkerhidden: View = <View>this.page.getViewById("placeamarker");
        placemarkerhidden.animate({
          translate:{x: 0 , y: -this.heightforanime },
          duration: 200
        });
      }
    }
    //READ IMAGES FOR THE UPLOADER FUNCTION
    public readimages(){
      if(isAndroid)
      {
        //DO ANDROID READ HERE
        const location =  android.provider.MediaStore.Images.Media.EXTERNAL_CONTENT_URI;
        const resolve = application.android.nativeApp.getContentResolver();
        var cursor = resolve.query(location,null,null,null,null);
        //THIS IS AN INTEGER
        if(cursor != null && cursor.moveToFirst())
        {
          var indexeddata = cursor.getColumnIndex(android.provider.MediaStore.Images.Media.DATA);
          do{
            var data = cursor.getString(indexeddata);
            this.potentialthumbnailimages.push(data);
          }while(cursor.moveToNext());
          console.log("images" + data);
        }
      }
      else
      {
        //do IOS READ HERE
      }
    }
    public tofolder;
    public async recordingoptfunction(args){
      try{
        const audioFolder = fs.knownFolders.currentApp().getFolder('audio');
        console.log(JSON.stringify(audioFolder));
        this.canrecord = true;
        let androidFormat;
        let androidEncoder;
        if(isAndroid)
        {
          androidFormat = 2;
          // androidEncoder = android.media.MediaRecorder.AudioEncoder.AAC;
          androidEncoder = 3;
        }
        const recordingPath = `${audioFolder.path}/recording.${this.platformExtension()}`;
        this.tofolder = recordingPath;

        const recorderOptions: AudioRecorderOptions = {
          filename: recordingPath,
          format: androidFormat,
          encoder: androidEncoder,
          metering: true,
          infoCallback: infoObject => {
            console.log(JSON.stringify(infoObject));
          },
          errorCallback: errorObject => {
            console.log(JSON.stringify(errorObject));
          }
        };
        this._recorder.start(recorderOptions);
        await this.playaudio(args);
        this.isrecording = true;
        if (recorderOptions.metering) {
          this._initMeter();
        }
      } catch (err) {
        this.isrecording = false;
        this._resetMeter();
        console.log(err);
      }
    }
    public functionstudiocompute(){
      if(this.currentholder === "")
      {
        this.feedback.show({
          title: "ERROR!",
          position: FeedbackPosition.Top, // iOS only
          type: FeedbackType.Warning, // this is the default type, by the way
          message: "no audio source...using raw recorder",
          duration: 3000,
          onTap: () => {
            this.feedback.hide();
          },
        });
      }
      else
      {
        var folder = fs.knownFolders.documents().path;
        var milliseconds = (new Date).getTime();
        var filename ="InfinityRealm" + milliseconds + ".mp4";
        var path = fs.path.join(folder , filename);
        var filename2 ="InfinityRealm" + milliseconds + ".mp3";
        var path2 = fs.path.join(folder , filename2);
        var filename3 ="InfinityRealm" + milliseconds + ".wav";
        var path3 = fs.path.join(folder , filename3);
        this.feedback.show({
          title: "ERROR!",
          position: FeedbackPosition.Top, // iOS only
          type: FeedbackType.Warning, // this is the default type, by the way
          message: "using " + this.currentholder.name + "as an audio source",
          duration: 3000,
          onTap: () => {
            this.feedback.hide();
          },
        });
        if(this.currentholder.type === "mp4/video")
        {
          FFmpeg.executeWithArguments(["-i", this.currentholder.data , path2])
          .then((result) => {
            console.log("FFmpeg process exited with result " + result)
            if(result === 0)
            {
              var millisecondsx = (new Date).getTime();
              var filenamex ="InfinityRealm" + milliseconds + ".mp3";
              var pathx = fs.path.join(folder , filenamex);console.log(pathx);
              FFmpeg.executeWithArguments(["-i", path2 , "-af" , "pan=stereo|c0=c0|c1=-1*c1" , "-ac" , "1", pathx])
              .then((result) => {
                console.log("FFmpeg process exited with result " + result)
                if(result === 0)
                {
                  var datatoplay = {
                    "category": "library",
                    "data" : pathx,
                    "name" : "sample",
                    "type" : this.mimeseparate,
                    "from" : "library",
                    "idr" : "library",
                    "uploader" : "uploader"
                  };
                  this.recordingoptfunction(datatoplay);
                  this.currentbeats = datatoplay.data;
                }
                else
                {
                  TNSFancyAlert.showError(
                    "Error!",
                    "Something went wrong could not use the audio file...",
                    "close"
                  ).then(() => {
                  });
                }
              });
            }
            else
            {
              TNSFancyAlert.showError(
                "Error!",
                "Something went wrong could not use the audio file...",
                "close"
              ).then(() => {
              });
            }
          });
        }
        else
        {
          var millisecondsx = (new Date).getTime();
          var filenamex ="InfinityRealm" + milliseconds + ".mp3";
          var pathx = fs.path.join(folder , filenamex);console.log(pathx);
          FFmpeg.executeWithArguments(["-i", this.currentholder.data , "-af" , "pan=stereo|c0=c0|c1=-1*c1" , "-ac" , "1", pathx])
          .then((result) => {
            console.log("FFmpeg process exited with result " + result)
            if(result === 0)
            {
              var datatoplay = {
                "category": "library",
                "data" : pathx,
                "name" : "sample",
                "type" : this.mimeseparate,
                "from" : "library",
                "idr" : "library",
                "uploader" : "uploader"
              };
              this.recordingoptfunction(datatoplay);
              this.currentbeats = datatoplay.data;
            }
            else
            {
              TNSFancyAlert.showError(
                "Error!",
                "Something went wrong could not use the audio file...",
                "close"
              ).then(() => {
              });
            }
          });
        }
      }
    }
    //FUNCTION TO START RECORDING
    public async startrecording(args){
      //paused
      if (!TNSRecorder.CAN_RECORD()) {
        this.canrecord = false;
        //dialogs.alert('This device cannot record audio.');
        return;
      }
      //handle arguments
      if(args.from === "studio")
      {console.log("STUDIO!!!!!!!!!!!!!!!!!");
        //audio is being recorded by the studio now dwtermine whether the beats used are from storage or are from online
        if(this.studiotype === "storage")
        {console.log("STORAGE!!!!!!!!!!!!!!!!!!! ");
          //do normally and remove the beats
          this.functionstudiocompute();
        }
        else
        {console.log("ONLINE!!!!!!!!!!!!!!!!!!!!");
          //no need to remove the beats becoz the audio is already a beat
          this.recordingoptfunction(this.currentholder);
        }
      }
    }
    public _meterInterval;
    public audioMeter = '0';
    private _initMeter() {
      this._resetMeter();
      this._meterInterval = setInterval(() => {
          this.audioMeter = this._recorder.getMeters();
          console.log(this.audioMeter);
      }, 300);
    }
    private _resetMeter() {
      if (this._meterInterval) {
        this.audioMeter = '0';
        clearInterval(this._meterInterval);
        this._meterInterval = undefined;
      }
    }
    public stopcompute(){
      this.mediaplayer.pause();
      var folder = fs.knownFolders.documents().path;
      var milliseconds2 = (new Date).getTime();
      var filename2 ="InfinityRealm" + milliseconds2 + ".mp3";
      var path2 = fs.path.join(folder , filename2);
      FFmpeg.executeWithArguments(["-i", this.tofolder , "-af" , "highpass=f=200,lowpass=f=3000" , path2])
      .then((result) => {
        console.log("FFmpeg process exited with result " + result)
        if(result === 0)
        {
          var milliseconds3 = (new Date).getTime();
          var filename3 ="InfinityRealm" + milliseconds3 + ".mp3";
          var path3 = fs.path.join(folder , filename3);
          FFmpeg.executeWithArguments(["-i", path2 , "-i" , this.currentbeats , "-filter_complex" , "amerge=inputs=2", "-ac" , "2", path3])
          .then((result) => {
            console.log("FFmpeg process exited with result " + result)
            if(result === 0)
            {
              var millisecondsx = (new Date).getTime();
              var filenamex ="InfinityRealm" + millisecondsx + ".mp3";
              var pathx = fs.path.join(folder , filenamex);
              FFmpeg.executeWithArguments(["-i" , path3 , "-filter:a" ,"loudnorm" , pathx])
              .then((result) => {
                console.log("FFmpeg process exited with result " + result)
                if(result === 0)
                {
                  this.tofolder = pathx;
                  TNSFancyAlert.showSuccess(
                    "Success!",
                    "successfully merged files...",
                    "close"
                  ).then(() => {
                  });
                }
                else
                {
                  TNSFancyAlert.showError(
                    "Error!",
                    "Something went wrong could not use the audio file...",
                    "close"
                  ).then(() => {
                  });
                }
              });
            }
            else
            {
              TNSFancyAlert.showError(
                "Error!",
                "Something went wrong could not use the audio file...",
                "close"
              ).then(() => {
              });
            }
          });
        }
        else
        {
          TNSFancyAlert.showError(
            "Error!",
            "Something went wrong could not use the audio file...",
            "close"
          ).then(() => {
          });
        }
      });

    }
    private async stoprecordinggg(){
      this._resetMeter();
      await this._recorder.stop().catch(ex => {
        console.log(ex);
        this.isrecording = false;
        this._resetMeter();
      });
      this.stopcompute();
      this.isrecording = false;
    }
    //FUNCTION TO STOP RECORDING
    public async stoprecording(){
    //  this.playing = true;
    //  this.mediaplayer.start();
      this._resetMeter();
      await this._recorder.stop().catch(ex => {
        console.log(ex);
        this.isrecording = false;
        this._resetMeter();
      });
      this.isrecording = false;
    }
    private platformExtension() {
      // 'mp3'
      return `${app.android ? 'm4a' : 'caf'}`;
    }
    //FUNCTION FOR CANT RECORD ANYTHING
    public cantrecordanything(){
      this.feedback.show({
        title: "Error!",
        position: FeedbackPosition.Top, // iOS only
        type: FeedbackType.Error, // this is the default type, by the way
        message: "Can't record on this device",
        duration: 3000,
        onTap: () => this.feedback.hide(),
      });
    }
    public samplerecording(){
      if(this.isrecording === true)
      {
        TNSFancyAlert.showInfo(
          "WAIT!",
          "recording in progress.",
          "close"
        ).then(() => {
        });
      }
      else
      { console.log(this.tofolder);
        var data = {
          "category": "library",
          "data" : this.tofolder,
          "name" : "sample recording",
          "type" : "audio/mp3",
          "from" : "library",
          "idr" : "library",
          "uploader" : "uploader"
        };
        this.playaudio(data);
      }
    }
    public discardrecording(){
      if(this.isrecording === true)
      {
        TNSFancyAlert.showInfo(
          "WAIT!",
          "recording in progress.",
          "close"
        ).then(() => {
        });
      }
      else
      {

      }
    }
    public useforcustomizing(args){
      if(this.isrecording === true)
      {
        TNSFancyAlert.showInfo(
          "WAIT!",
          "recording in progress.",
          "close"
        ).then(() => {
        });
      }
      else
      {
        var folder = fs.knownFolders.documents().path;
        var milliseconds = (new Date).getTime();
        var filename ="InfinityRealm" + milliseconds + ".mp4";
        var path = fs.path.join(folder , filename);
        var filename2 ="InfinityRealm" + milliseconds + ".mp3";
        var path2 = fs.path.join(folder , filename2);
        var filename3 ="InfinityRealm" + milliseconds + ".wav";
        var path3 = fs.path.join(folder , filename3);
        //first find out if we are working with an audio or a video input
        if(this.mimeseparate === "video/avi" || this.mimeseparate === "video/flv" || this.mimeseparate === "video/mp4" || this.mimeseparate === "video/mov" || this.mimeseparate === "video/wmv" || this.mimeseparate === "video/webm" || this.mimeseparate === "video/webvtt" || this.mimeseparate === "video/ogt")
        {
          if(args.type === "audio/mp3")
          {
            setTimeout(()=>{
              FFmpeg.executeWithArguments(["-i", this.pathtofile , "-i" , args.data , "-c:v" , "copy", "-map" , "0:v:0" , "-map" , "1:a:0", "-shortest" , path])
              .then((result) => {
                console.log("FFmpeg process exited with result " + result)
                if(result === 0)
                {
                  var datatoplay = {
                    "category": "library",
                    "data" : path,
                    "name" : "sample",
                    "type" : this.mimeseparate,
                    "from" : "library",
                    "idr" : "library",
                    "uploader" : "uploader"
                  };
                  this.playaudio(datatoplay);
                }
                else
                {
                  TNSFancyAlert.showError(
                    "Error!",
                    "Something went wrong...could not merge that video with the audio...we will fix it..",
                    "close"
                  ).then(() => {
                  });
                }
              });
            },1000);
            FFmpeg.getLastReceivedStatistics().then(stats => console.log('Stats: ' + JSON.stringify(stats)));
          }
          else
          {
            //first extract the audio of the video file and use the same command to merge
            setTimeout(()=>{
              FFmpeg.executeWithArguments(["-i", args.data , path2])
              .then((result) => {
                console.log("FFmpeg process exited with result " + result)
                if(result === 0)
                {
                  FFmpeg.executeWithArguments(["-i", this.pathtofile , "-i" , path2 , "-c:v" , "copy", "-map" , "0:v:0" , "-map" , "1:a:0", "-shortest" , path])
                  .then((result) => {
                    console.log("FFmpeg process exited with result " + result)
                    if(result === 0)
                    {
                      var datatoplay = {
                        "category": "library",
                        "data" : path,
                        "name" : "sample",
                        "type" : this.mimeseparate,
                        "from" : "library",
                        "idr" : "library",
                        "uploader" : "uploader"
                      };
                      this.playaudio(datatoplay);
                    }
                    else
                    {
                      TNSFancyAlert.showError(
                        "Error!",
                        "Something went wrong could not merge the encoded video with the exctracted audio",
                        "close"
                      ).then(() => {
                      });
                    }
                  });
                }
                else
                {
                  TNSFancyAlert.showError(
                    "Error!",
                    "Something went wrong could not exctract audio from the video file",
                    "close"
                  ).then(() => {
                  });
                }
              });
            },1000);
            FFmpeg.getLastReceivedStatistics().then(stats => console.log('Stats: ' + JSON.stringify(stats)));
          }
        }
        /*else
        {
          //assuming we used the audio library add one that gets the audio from a video
          //first make a copy but is it really necessary first lets work with an overwrittable output file since we cant overwrite the input file
          if(args.type === "video/mp4")
          {
            setTimeout(()=>{
              FFmpeg.executeWithArguments(["-i", args.data , path2])
              .then((result) => {
                console.log("FFmpeg process exited with result " + result)
                if(result === 0)
                {
                  FFmpeg.executeWithArguments(["-i", this.pathtofile , "-i" , path2 , "-filter_complex" , "concat=n=2:v=0:a=1[a]", "-map" , "[a]" , path3])
                  .then((result) => {
                    console.log("FFmpeg process exited with result " + result)
                    if(result === 0)
                    {
                      var datatoplay = {
                        "category": "library",
                        "data" : path3,
                        "name" : "sample",
                        "type" : this.mimeseparate,
                        "from" : "library",
                        "idr" : "library",
                        "uploader" : "uploader"
                      };
                      this.playaudio(datatoplay);
                    }
                    else
                    {
                      TNSFancyAlert.showError(
                        "Error!",
                        "Something went wrong could not use the extracted audio",
                        "close"
                      ).then(() => {
                      });
                    }
                  });
                }
                else
                {
                  TNSFancyAlert.showError(
                    "Error!",
                    "Something went wrong could not excract the audio from the video file",
                    "close"
                  ).then(() => {
                  });
                }
              });
            },1000);
            FFmpeg.getLastReceivedStatistics().then(stats => console.log('Stats: ' + JSON.stringify(stats)));
          }
          else
          { //combination of files command
        //  FFmpeg.executeWithArguments(["-i", this.pathtofile , "-i" , args.data , "-filter_complex" , "concat=n=2:v=0:a=1[a]", "-map" , "[a]", "-codec:a" , "libmp3lame" , "-q:a" , "3" , path2])
        // command to merge 2 audio FFmpeg.executeWithArguments(["-i", this.pathtofile , "-i" , args.data , "-filter_complex" , "amerge=inputs=2", "-ac" , "2", path2])
        // remove vocals working kinda FFmpeg.executeWithArguments(["-i", this.pathtofile , "-af" , "pan=stereo|c0=c0|c1=-1*c1" , "-ac" , "1", path2])

            setTimeout(()=>{
              FFmpeg.executeWithArguments(["-i", this.pathtofile , path3])
              .then((result) => {
                console.log("FFmpeg process exited with result " + result)
                if(result === 0)
                {console.log(path3);
                  var millisecondsx = (new Date).getTime();
                  var filenamex ="InfinityRealm" + milliseconds + ".mp3";
                  var pathx = fs.path.join(folder , filenamex);console.log(pathx);
                  FFmpeg.executeWithArguments(["-i", path3 , "-af" , "pan=stereo|c0=c0|c1=-1*c1" , "-ac" , "1", pathx])
                  .then((result) => {
                    console.log("FFmpeg process exited with result " + result)
                    if(result === 0)
                    {
                      var datatoplay = {
                        "category": "library",
                        "data" : pathx,
                        "name" : "sample",
                        "type" : this.mimeseparate,
                        "from" : "library",
                        "idr" : "library",
                        "uploader" : "uploader"
                      };
                      this.playaudio(datatoplay);
                    }
                    else
                    {
                      TNSFancyAlert.showError(
                        "Error!",
                        "Something went wrong could not use the audio file...",
                        "close"
                      ).then(() => {
                      });
                    }
                  });
                }
                else
                {
                  TNSFancyAlert.showError(
                    "Error!",
                    "Something went wrong could not use the audio file...",
                    "close"
                  ).then(() => {
                  });
                }
              });

            },1000);
            FFmpeg.getLastReceivedStatistics().then(stats => console.log('Stats: ' + JSON.stringify(stats)));
          }
        }*/
      }
    }
    public recorderlaunch(){}
    public studiolaunch(){
      let newobj: View = <View>this.page.getViewById("studiotemp");
      newobj.animate({
          translate:{x: 0 , y: 0},
          duration: 200
      });
    }
    public hidestudio(args :SwipeGestureEventData){
      let studiotemp: View = <View>this.page.getViewById("studiotemp");
      if(this.isrecording === true)
      {
        this.stoprecording();
        TNSFancyAlert.showInfo(
          "Stopped recording!",
          "recording was stopped.",
          "close"
        ).then(() => {
        });
      }
      if(args.direction === 8){
        //take down
        studiotemp.animate({
          translate:{x: 0 , y: -this.heightforanime },
          duration: 200
        });
      }
      else if(args.direction === 4)
      {
        //swiping up
        studiotemp.animate({
          translate:{x: 0 , y: this.heightforanime },
          duration: 200
        });
      }
      else if(args.direction === 1)
      {
        //swiping right
        studiotemp.animate({
          translate:{x: this.widthforanime , y : 0},
          duration: 200
        });
      }
      else
      {
        //swiping left
        studiotemp.animate({
          translate:{x: -this.widthforanime , y : 0},
          duration: 200
        });
      }
    }
    public onLoadMoreItemsRequested(args: ListViewEventData) {
        var that = new WeakRef(this);
        setTimeout(() => {
            var listView: RadListView = args.object;
            var initialNumberOfItems = that.get()._numberOfAddedItems;
            for (var i = that.get()._numberOfAddedItems; i < initialNumberOfItems + 2; i++) {
                if (i > mypostdata.length - 1) {
                    listView.loadOnDemandMode = ListViewLoadOnDemandMode[ListViewLoadOnDemandMode.None];
                    break;
                }

                that.get().categories.push(mypostdata[i]);
                that.get()._numberOfAddedItems++;
            }

            listView.notifyLoadOnDemandFinished();
        }, 5000);
        args.returnValue = true;
    }
    //CALCULATIONS
    public newvidwidth;
    public async myvideocalculations(args){console.log(this.classvideo);
      this.vidintervals = setInterval(()=>{
        //change milliseconds to seconds
        let curr = this.classvideo.getCurrentTime() * 0.001;
        let durr = this.classvideo.getDuration() * 0.001;
        let perc = curr / durr * 100;
        this.sliderValuevid = perc;
        this.newvidwidth = perc;
        let minutescurr= Math.floor(curr / 60);
        let secondscurr= Math.floor(curr % 60);
        let a = secondscurr.toString();
        if(secondscurr < 10)
        {
          a = "0" + secondscurr.toString();
        }
        console.log(minutescurr + " + " + curr + " + " + secondscurr + "+" + a);
        this.tracetime = minutescurr + ":" + a;
        this.time = curr;
        this.newduration = durr;
      },1000);
      let time = parseInt(this.classvideo.getDuration()) * 60 / 100;
      this.viewtimeout =  setTimeout(()=>{
        console.log("instert viewd at time " + time);
      },time);
      this.classvideo.on(Video.finishedEvent, () => {
        this.adanimate(args);
      });

    }
    //CALCULATIONS
    public async myaudiocalculations(args){
      /*worker.postMessage(args);
      worker.onmessage = (msg) => {
        if (msg.data.success) {
          if(msg.data.width === 100)
          {
            //worker.terminate();
            this.adanimate(args);
          }
          else
          {
            if(msg.data.width === 99)
            {
              this.adanimate(args);
            }
            else
            {
              if(msg.data.from === "play")
              {
                this.mynewwidth = msg.data.width;
                this.sliderValue1 = msg.data.width;
                this._changeDetectionRef.detectChanges();
                console.log(this.mynewwidth + "rrt");
              }
              else if(msg.data.from === "pause")
              {
                this.playing = msg.data.playstatus;
              }
              else if(msg.data.from === "destroy")
              {
                worker.terminate();
              }
            }
          }
        } else {
            // Stop idle animation
            // Display meaningful message
            // Terminate worker or send message with different parameters
        }
      }*/

      this.intervals = setInterval(()=>{
        //change milliseconds into seconds if android
        let time = this.mediaplayer.currentTime * 0.001;
        let myduration = parseInt(this.tnsduration);
        //change milliseconds into seconds if android
        let newduration = myduration * 0.001;
        let division = time / newduration;
        let newwidth = division * 100;
        this.time = time;
        this.newduration = newduration;
        //set the width
        this.sliderValue1 = newwidth;
        this.mynewwidth = newwidth;
        //fetch event that audio has finished playing
        this._changeDetectionRef.detectChanges();
        console.log(division + " " + this.tnsduration + " " + time);
        //this.fetchnextvideo({mimetype : args.type , from : args.from ,category : args.category , data : args.data ,idr : args.idr});
      },1000);
      let time = parseInt(this.tnsduration) * 60 / 100;
      this.viewtimeout = setTimeout(()=>{
        if(this.tnsduration < 0)
        {
          TNSFancyAlert.showError(
            "Suspicious Activity!",
            "Something might be wrong with this post",
            "close"
          ).then(() => {

          });
          this.reportingfunc({postid:this.currentdata,type:'suspicious'});
        }
        else
        { console.log("AT TIME" + time);
          let timeinperc = time / parseInt(this.tnsduration) * 100;
          const idsummary = {
            myid : this.myid,
            from : this.imcurrentlylisteningto.from,
            time : timeinperc,
            timeinsec :  this.mediaplayer.currentTime * 0.001,
            duraton :  parseInt(this.tnsduration) * 0.001,
            uploaderid: this.mycurrentsrc,
            postid: this.currentdata,
          };
          this.homeservice.views(idsummary)
              .subscribe((data)=>{
                if(data.error === "error")
                {
                  console.log("error occured");
                }
                else
                {
                  TNSFancyAlert.showSuccess(
                    "Updated!",
                    "Successfuly updated.",
                    "close"
                  ).then(() => {
                  });
                }
              },(error)=>{

              });
        }

      },time);

    }
    public _trackComplete(){
      this.adanimate(this.imcurrentlylisteningto);
    }
    public _trackError(){
      console.log("error");
    }
    //SEEK
    public seekingvid($event){
      let ans = this.classvideo.getDuration() * ($event.value / 100);
      this.classvideo.seekToTime(ans);
      console.log($event.value);
    }
    //reporting function
    public reportingfunc(args){
      const id = {
        postid: args.postid,
        reporttype : args.type,
        reporterid : this.myid,
      }
      //get summary
      this.homeservice.report(id)
          .subscribe((data)=>{
            if(data.code === "errordata")
            {
              TNSFancyAlert.showError(
                "Error!",
                "Internal server error we will fix it",
                "close"
              ).then(() => {

              });
            }
            else
            {
              //notify the owner

              TNSFancyAlert.showSuccess(
                "Success!",
                "Thank you for reporting...",
                "close"
              ).then(() => {

              });
            }
          },(error)=>{
              TNSFancyAlert.showError(
                "Error!",
                "Internal server error we will fix it",
                "close"
              ).then(() => {

              });
          });
    }

    //FUNCTION TO LOG OUT SPEAKS FOR ITSELF
    public logout(){
      try{
        this.mediaplayer.dispose();
        clearInterval(this.intervals);
        clearInterval(this.vidintervals);
      }
      catch(error){
        console.log("could not destroy");
      }
      //this.categories.length = 0;
      mypostdata.length = 0;
      application.android.startActivity.getWindow().clearFlags(android.view.WindowManager.LayoutParams.FLAG_LAYOUT_NO_LIMITS);
      this.routerextensions.navigate(['/'], {
        clearHistory: true,
        transition: {
          name: 'slideLeft',
          duration: 900,
          curve: 'easeOut'
        }
      });
      appSettings.clear();
    }
    // css colour temp   background: linear-gradient(to bottom, #cc0099 0%, #660066 84%);

}
