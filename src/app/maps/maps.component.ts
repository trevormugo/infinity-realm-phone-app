import { Component,OnInit } from "@angular/core";
import { Page } from "tns-core-modules/ui/page";
import { Adress } from "../ip.service";
import { screen } from "tns-core-modules/platform"
import { RouterExtensions } from "nativescript-angular/router";
import { DatePicker } from "tns-core-modules/ui/date-picker";
import { View } from "tns-core-modules/ui/core/view";
import { SwipeGestureEventData } from "tns-core-modules/ui/gestures";
declare var android: any; // <- important! avoids namespace issues
import { registerElement } from "nativescript-angular/element-registry";
registerElement("Mapbox", () => require("nativescript-mapbox").MapboxView);
import * as Toast from 'nativescript-toast';
import { HomeService } from "../home.service";
import * as appSettings from "tns-core-modules/application-settings";

@Component({
    selector: "ns-login",
    providers: [Adress],
    moduleId: module.id,
    templateUrl: "./maps.component.html",
    styleUrls: ["./maps.component.css"],
})
export class MapsComponent implements OnInit {
    public query;
    public id = appSettings.getString("idr");
    public shouldlisten = false;
    public description;
    public name;
    public minDate: Date = new Date(1975, 0, 29);
    public maxDate: Date = new Date(4019, 4, 12);
    public mappublic;
    public daytext = "day";
    public monthtext = "month";
    public yeartext = "year";
    public addlat;
    public addlong;
    public mypoints;
    public myfollowingpoints;
    public widthforanime = screen.mainScreen.widthDIPs;
    public heightforanime = -screen.mainScreen.heightDIPs;
    ngOnInit() {
      this.page.actionBarHidden = true;

    }
    constructor(public homeservice : HomeService , public page : Page) {

    }
    public onMapReady(args){
      console.log("map is ready");
      console.log(args);
      this.mappublic = args.map;
      this.addmarkers();
    }
    public addmarkers(){
      var myid = appSettings.getString("idr");
      const data = {
        id: myid,
      };
      this.homeservice.getmapsdata(data)
            .subscribe((res) => {
              //lets find out the number of points that belong to me
              this.mypoints = res.me.length;
              //the number of points that belong to the people i follow
              this.myfollowingpoints = res.following.length;
              //add the markers that belong to me
              res.me.forEach((element)=>{
                setTimeout(()=>{
                  this.mappublic.addMarkers([
                  {
                    lat: element.latitude,
                    lng: element.longitude,
                    title: element.nameofevent,
                    subtitle: element.date,
                    selected: true, // makes the callout show immediately when the marker is added (note: only 1 marker can be selected at a time)
                    onTap: function(marker){
                      console.log(marker);
                    }
                  }
                ]);
              },4000);
              });
              //add the markers that belong to my following
              res.following.forEach((element)=>{
                setTimeout(()=>{
                  this.mappublic.addMarkers([
                  {
                    lat: element.latitude,
                    lng: element.longitude,
                    title: element.nameofevent,
                    subtitle: element.date,
                    selected: true, // makes the callout show immediately when the marker is added (note: only 1 marker can be selected at a time)
                    onTap: function(marker){
                      console.log(marker);
                    }
                  }
                ]);
              },4000);
              });
            },(error) => {
                console.log("error" + error);
            })
    }
    public addlocation(){
      //logic to add location
      this.shouldlisten = true;
      console.log("tapping");
      //first we will remove all maerkers
      this.mappublic.removeMarkers();
      var toast = Toast.makeText("tap on the map to add a marker");
      toast.show();
      this.mappublic.setOnMapClickListener((point)=>{
        if(this.shouldlisten === false)
        {
          //do nothing
        }
        else
        {
          this.addlat = point.lat;
          this.addlong = point.lng;
          var toast = Toast.makeText("latitude:"+point.lat+" longitude:"+point.lng);
          toast.show();
          //show the upload box then add the markers back to the map
          let placemarkerhidden: View = <View>this.page.getViewById("placeamarker");
          placemarkerhidden.animate({
            translate:{x: 0 , y: 0 },
            duration: 200
          });
        }
      });
    }
    public onDayChanged(args) {
      this.daytext = args.value;
    }
    public onMonthChanged(args) {
      this.monthtext = args.value;
    }
    public onYearChanged(args) {
      this.yeartext = args.value;
    }
    public hidemarkuploader(args : SwipeGestureEventData){
      let placemarkerhidden: View = <View>this.page.getViewById("placeamarker");
      if(args.direction === 8){
        //take down
        placemarkerhidden.animate({
          translate:{x: 0 , y: -this.heightforanime },
          duration: 200
        });
        var toast = Toast.makeText("cancelling...");
        toast.show();
        this.daytext = "day";
        this.monthtext = "month";
        this.yeartext = "year";
        this.name = "";
        this.description = "";
        //return all markers
        this.addmarkers();
        this.shouldlisten = false;
      }
    }
    public tappedhide(){
      let placemarkerhidden: View = <View>this.page.getViewById("placeamarker");
      placemarkerhidden.animate({
        translate:{x: 0 , y: -this.heightforanime },
        duration: 200
      });
      var toast = Toast.makeText("cancelling...");
      toast.show();
      this.daytext = "day";
      this.monthtext = "month";
      this.yeartext = "year";
      this.name = "";
      this.description = "";
      //return all markers
      this.addmarkers();
      this.shouldlisten = false;
    }
    public placemark(){
      console.log("placing marker");
      if(!this.name || this.daytext === "day" || this.monthtext === "month" || this.yeartext === "year")
      {
        //fill in all required fields
        var toast = Toast.makeText("please fill in all required fields");
        toast.show();
      }
      else
      {
        this.shouldlisten = false;
        console.log("send to database");
        //return all markers
        setTimeout(()=>{this.addmarkers()},4000);
        //logic to add marker goes here.using sockets
        var data = {
          pointowner: this.id,
          longitude: this.addlong,
          latitude: this.addlat,
          date: this.daytext+"/"+this.monthtext+"/"+this.yeartext,
          descriptionofevent: this.description,
          nameofevent: this.name,
        };
        this.homeservice.map(data);
        var toast = Toast.makeText("markers have been updated");
        toast.show();
        //close the window
        this.daytext = "";
        this.monthtext = "";
        this.yeartext = "";
        this.name = "";
        this.description = "";
        let placemarkerhidden: View = <View>this.page.getViewById("placeamarker");
        placemarkerhidden.animate({
          translate:{x: 0 , y: -this.heightforanime },
          duration: 200
        });
      }
    }
}
