import { Injectable } from "@angular/core";
import { HttpClient , HttpHeaders} from "@angular/common/http";
import { Observable , throwError } from "rxjs";
import { retry ,catchError } from "rxjs/operators";
import { Adress } from "./ip.service";
@Injectable()
export class LoginService {
  url = this.outurl.getIp();
  constructor(private http : HttpClient , public outurl : Adress){}
  //LOG IN
  loginReq(logininfo):Observable <any> {
    const httpOptions = {
         headers : new HttpHeaders({
            "Content-Type":  "application/json",
            "Authorization": "trevor",
            "Access-Control-Allow-Origin" : "*"
         })
     }
    return this.http.post<any>(this.url + "/infinitelogin",logininfo,httpOptions)
      .pipe(
        retry(3),
      );
  }
  //SEND CODE TO FORGOTTEN PASSWORDS
  send(myemail):Observable <any> {
    const httpOptions = {
         headers : new HttpHeaders({
            "Content-Type":  "application/json",
            "Authorization": "trevor",
            "Access-Control-Allow-Origin" : "*"
         })
     }
    return this.http.post<any>(this.url + "/resent",myemail,httpOptions)
      .pipe(
        retry(3),
      );
  }

  //CHANGE THE PASSWORD
  changepass(newdata):Observable <any> {
    const httpOptions = {
         headers : new HttpHeaders({
            "Content-Type":  "application/json",
            "Authorization": "trevor",
            "Access-Control-Allow-Origin" : "*"
         })
     }
    return this.http.post<any>(this.url + "/newpassword",newdata,httpOptions)
      .pipe(
        retry(3),
      );
  }


}
