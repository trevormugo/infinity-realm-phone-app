import { Injectable } from "@angular/core";
import { HttpClient , HttpHeaders} from "@angular/common/http";
import { Observable , throwError } from "rxjs";
import { retry ,catchError } from "rxjs/operators";
import { Adress } from "./ip.service";
@Injectable()
export class SignupService {
  url = this.outurl.getIp();
  constructor(private http : HttpClient , public outurl : Adress){}

  createaccount(accountinfo):Observable <any> {
    const httpOptions = {
         headers : new HttpHeaders({
            "Content-Type":  "application/json",
            "Authorization": "trevor",
            "Access-Control-Allow-Origin" : "*"
         })
     }
    return this.http.post<any>(this.url + "/infinitesignup",accountinfo,httpOptions)
      .pipe(
        retry(3),
      );
  }

  //route to delete the unverified accounts using the email adress as the refernce
  deletetheaccount(deletetheinfo):Observable <any> {
    const httpOptions = {
         headers : new HttpHeaders({
            "Content-Type":  "application/json",
            "Authorization": "trevor",
            "Access-Control-Allow-Origin" : "*"
         })
     }
    return this.http.post<any>(this.url + "/infinitedeletion",deletetheinfo,httpOptions)
      .pipe(
        retry(3),
      );
  }

  //route to get the signature ie the user id;
  sign(emaildata):Observable <any> {
    const httpOptions = {
         headers : new HttpHeaders({
            "Content-Type":  "application/json",
            "Authorization": "trevor",
            "Access-Control-Allow-Origin" : "*"
         })
     }
    return this.http.post<any>(this.url + "/signature",emaildata,httpOptions)
      .pipe(
        retry(3),
      );
  }


}
