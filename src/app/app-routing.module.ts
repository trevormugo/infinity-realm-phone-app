import { NgModule } from "@angular/core";
import { NativeScriptRouterModule } from "nativescript-angular/router";
import { Routes } from "@angular/router";
import { LoginComponent } from "./login/login.component";
import { SignupComponent } from"./signup/signup.component";
import { PwdComponent } from "./passwordreset/passwordreset.component";
import { VerifyComponent } from "./passwordreset/verify.component";
import { ActchgeComponent } from"./passwordreset/actchge.component";
import { VerifyEmailComponent } from "./signup/verifyemail.component";
import { ProfilepicComponent } from "./signup/profilepicture.component";
import { HomePageComponent } from "./home/home.component";
import { MapsComponent } from "./maps/maps.component";

const routes: Routes = [
    { path: "" , component: LoginComponent },
    { path: "signup" , component: SignupComponent },
    { path: "actchge", component: ActchgeComponent },
    { path: "uploadpic" , component: ProfilepicComponent },
    { path: "maps" , component: MapsComponent },
    { path: "verifyemail" , component: VerifyEmailComponent },
    { path: "pwd" , component: PwdComponent },
    { path: "home" , component: HomePageComponent },
    { path: "verify" , component: VerifyComponent },
];

@NgModule({
    imports: [NativeScriptRouterModule.forRoot(routes)],
    exports: [NativeScriptRouterModule]
})
export class AppRoutingModule { }
