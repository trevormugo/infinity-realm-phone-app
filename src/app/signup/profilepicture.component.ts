import { Component, OnInit } from "@angular/core";
import { Page } from "tns-core-modules/ui/page";
import { RouterExtensions } from "nativescript-angular/router";
import { SignupService } from "../signup.service";
import { TNSFancyAlert, TNSFancyAlertButton } from "nativescript-fancyalert";
import { Adress } from "../ip.service";
import * as appSettings from "tns-core-modules/application-settings";
import * as imagepicker from "nativescript-imagepicker";
import * as camera from "nativescript-camera";
import { Image } from "tns-core-modules/ui/image";
//import { ImageCropper } from "nativescript-imagecropper";
import * as imageSource from "tns-core-modules/image-source";
import * as frameModule from "tns-core-modules/ui/frame";
//import {Folder, path, knownFolders} from "tns-core-modules/file-system";
const fs = require("file-system");
const bghttp = require("nativescript-background-http");
const permissions = require('nativescript-permissions');
declare var android: any; // <- important! avoids namespace issues
@Component({
    selector: "ns-signup",
    providers: [ Adress , SignupService ],
    moduleId: module.id,
    templateUrl: "./profilepicture.component.html",
    styleUrls: ["./profilepicture.component.css"],
})
export class ProfilepicComponent implements OnInit {
    public activity:Boolean = false;
    public showbtn:Boolean = true;
    public myimagetesing;
    public mine;
    //public imageCropper: ImageCropper;
    private imageSource: imageSource.ImageSource;
    private imagepathtoupload:String;
    private imagenametoupload:String;
    ngOnInit() {
        this.page.actionBarHidden = true;
        setTimeout(() => {
          this.mine = frameModule.topmost().getViewById("croppedImage");
        }, 1000);
        const checkidr = appSettings.getString("idr");
        const checkcode = appSettings.getString("mycode");
        if(!checkidr)
        {
          //if there is no  referer to the email adress we will get the signature after this;
          //the idr is our referer the unique id;
          TNSFancyAlert.showError(
            "Error!",
            "something went wrong try loging in.",
            "close"
          ).then(() => {
              console.log("info not there");
              this.routerextensions.back();
          });
        }
        else if(checkcode)
        {
          appSettings.remove("mycode");
        }
        else
        {
          this.myimagetesing = this.myip.getIp() + "/profilenoneupload/" + checkidr;
          if(!permissions.hasPermission(android.Manifest.permission.CAMERA))
          {
            this.camerapermission();
          }
          else
          {
            if(!permissions.hasPermission(android.Manifest.permission.WRITE_EXTERNAL_STORAGE))
            {
              this.writeexternalpermission();
            }
            else
            {
              //do nothing since the camera is initiated by the user
            }
          }
        }
    }
    constructor( public myip : Adress , public signuping : SignupService , public page : Page , public routerextensions : RouterExtensions) {
      //this.imageCropper = new ImageCropper();
    }
    public camerapermission(){
      permissions.requestPermission(android.Manifest.permission.CAMERA, "I need these permissions for the user sake")
      .then( () => {
        console.log("permission granted");
        if(!permissions.hasPermission(android.Manifest.permission.WRITE_EXTERNAL_STORAGE))
        {
          this.writeexternalpermission();
        }
        else
        {
          //donothing since permission already granted
        }
      })
      .catch( () => {
        console.log("no permission");
        //shrow error and reload page
        TNSFancyAlert.showError(
          "Error!",
          "We need you to accept this request to use the camera.",
          "close"
        ).then(() => {
          this.camerapermission();
        });
      });
    }
    public writeexternalpermission(){
      permissions.requestPermission(android.Manifest.permission.WRITE_EXTERNAL_STORAGE, "I need these permissions for the user sake")
      .then( () => {
        console.log("permission granted");
      })
      .catch( () => {
        console.log("no permission");
        //shrow error and reload page
        TNSFancyAlert.showError(
          "Error!",
          "We need you to accept this request to use the camera.",
          "close"
        ).then(() => {
          this.writeexternalpermission();
        });
      });
    }
    public togallery(){
      let context = imagepicker.create({
          mode: "single" // use "multiple" for multiple selection
      });
      context.authorize().then(() => {
          return context.present();
        }).then((selection) => {
              selection.forEach((selected) => {
                selected.getImageAsync(source => {
                const selectedImgSource = imageSource.fromNativeSource(source);
                /*this.imageCropper
                    .show(selectedImgSource, { width : 500 ,height: 500 , lockSquare: true })
                    .then(args => {
                      //instead of doing this let us save it in a file;
                      //all the images will be saved in a file for the meanwhile we can delete them if we wanted
                      //but let us just hold off on that.
                      var milliseconds = (new Date).getTime();
                      var folder = fs.knownFolders.documents().path;
                      var filename ="InfinityRealm" + milliseconds + ".png";
                      var path = fs.path.join(folder , filename);
                      var saved = args.image.saveToFile(path , "png");
                      //save to global variabes
                      this.imagepathtoupload = path;
                      this.imagenametoupload =  path.substr(path.lastIndexOf("/") + 1);
                      this.myimagetesing = path;
                      console.log("my path " + path);
                      if (saved) {
                        this.myimagetesing = path;
                        console.log(path);
                      }
                    })
                    .catch(function(e) {
                        //the cropper didnt launch configure errors;
                        console.log(e);
                    });*/
            });
              });
        }).catch((e) => {
          // process error
          console.log(e);
        });
    }
    public tocamera(){
      var options = { width : 700 ,height: 700, keepAspectRatio: true, saveToGallery: false };
      camera.takePicture(options)
          .then(imageAsset => {
              console.log(imageAsset);
              let source = new imageSource.ImageSource();
                         source.fromAsset(imageAsset).then((source) => {
                             /*this.imageCropper
                                 .show(source,{ width : 500 ,height: 500 , lockSquare: true })
                                 .then(args => {
                                          //instead of doing this let us save it in a file;
                                          var milliseconds = (new Date).getTime();
                                          var folder = fs.knownFolders.documents().path;
                                          var filename ="InfinityRealm" + milliseconds + ".png";
                                          var path = fs.path.join(folder , filename);
                                          var saved = args.image.saveToFile(path , "png");
                                          //save to global variabes
                                          this.imagepathtoupload = path;
                                          this.imagenametoupload =  path.substr(path.lastIndexOf("/") + 1);
                                          this.myimagetesing = path;
                                          console.log(this.imagenametoupload + " , " + this.imagepathtoupload);
                                          console.log("my path " + path);
                                          if (saved) {
                                            this.myimagetesing = path;
                                            console.log(path);
                                          }
                                 })
                                 .catch(function(e) {
                                      //there was somekind of error redo it again;
                                     console.dir(e);
                                 });*/

                               });
          }).catch((err) => {
              //process error
              console.log("Error -> " + err.message);
          });
    }
    public later(){
      if(this.activity === true)
      {
        TNSFancyAlert.showInfo(
          "Info!",
          "Please wait until current process is finished.",
          "close"
        ).then(() => {

        });
      }
      else
      {
        this.routerextensions.navigate(['/home'], {
          clearHistory: true,
          transition: {
            name: 'slideLeft',
            duration: 900,
            curve: 'easeOut'
          }
        });
      }
    }
    //upload the chosen file
    public uploadchosen(){
      this.activity = true;
      this.showbtn = false;
      let session = bghttp.session("image-upload");
      let params = appSettings.getString("idr");
      let request = {
          url: this.myip.getIp()+"/newprofilepicture/"+params,
          method: "POST",
          description: "Uploading " + this.imagenametoupload
      };
      let params2 = [{ "name": "file" , "filename": this.imagepathtoupload, "mimeType": "image/png" }];
      let task = session.multipartUpload(params2, request);
      //var task = session.uploadFile(this.imagepathtoupload, request);
      //after the upload process is complete do this.
      task.on("error", ()=>{
        TNSFancyAlert.showError(
          "Error!",
          "Profile not uploaded try again later.",
          "close"
        ).then(() => {
            //navigate to the next page it will be the home page;
            this.activity = false;
            this.showbtn  = true;
            this.routerextensions.navigate(['/home'], {
              clearHistory: true,
              transition: {
                name: 'slideLeft',
                duration: 900,
                curve: 'easeOut'
              }
            });
        });
      });

      task.on("progress", (event)=>{
        let current = event.currentBytes;
        let total = event.totalBytes;
        let division = current / total;
        let percentage = division * 100;
        console.log(percentage + "%");
      });

      task.on("complete",(event)=>{
        TNSFancyAlert.showSuccess(
          "Success",
          "Profile image updated.",
          "close"
        ).then(() => {
            //navigate to the next page it will be the home page;
            this.activity = false;
            this.showbtn  = true;
            this.routerextensions.navigate(['/home'], {
              clearHistory: true,
              transition: {
                name: 'slideLeft',
                duration: 900,
                curve: 'easeOut'
              }
            });
        });
      });
    }

}
