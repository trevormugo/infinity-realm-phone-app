import { Component, OnInit } from "@angular/core";
import { Page } from "tns-core-modules/ui/page";
import { RouterExtensions } from "nativescript-angular/router";
import { SignupService } from "../signup.service";
import { TNSFancyAlert, TNSFancyAlertButton } from "nativescript-fancyalert";
import { Adress } from "../ip.service";
import * as appSettings from "tns-core-modules/application-settings";

@Component({
    selector: "ns-signup",
    providers: [ Adress , SignupService ],
    moduleId: module.id,
    templateUrl: "./verifyemail.component.html",
    styleUrls: ["./verifyemail.component.css"],
})
export class VerifyEmailComponent implements OnInit {
    public verification:String;
    public counter = 0;
    public activity:Boolean = false;
    public showbtn:Boolean = true;
    ngOnInit() {
        this.page.actionBarHidden = true;
        const checkemail = appSettings.getString("myemail");
        const checkcode = appSettings.getString("mycode");
        if(!checkcode || !checkemail)
        {
          TNSFancyAlert.showError(
            "Error!",
            "something went wrong.",
            "close"
          ).then(() => {
              console.log("info not there");
              this.routerextensions.back();
          });
        }
    }
    constructor( public signuping : SignupService , public page : Page , public routerextensions : RouterExtensions) { }

    public credentials(){
      //DELETE THE INFO
      this.activity = true;
      this.showbtn = false;
      var emaildel = appSettings.getString("myemail");
      const datatodelete = {
        email : emaildel,
      }
      this.signuping.deletetheaccount(datatodelete)
          .subscribe((res)=>{
            if(!res)
            {
              TNSFancyAlert.showError(
                "Error!",
                "something went wrong you cannot go back.",
                "close"
              ).then(() => {
                   this.showbtn = true;
                   this.activity = false;
              });
            }
            else if(res.delete === "not")
            {
              TNSFancyAlert.showError(
                "Error!",
                "something went wrong.",
                "close"
              ).then(() => {
                   this.showbtn = true;
                   this.activity = false;
              });
            }
            else if(res.delete === "deleted")
            {
              //successfully been deleted
              //remove code from appsettings
              appSettings.remove("mycode");
              appSettings.remove("myemail");
              this.routerextensions.back();
              this.showbtn = true;
              this.activity = false;
            }
          },(error)=>{
            TNSFancyAlert.showError(
              "Error!",
              "something went wrong.",
              "close"
            ).then(() => {
                 this.showbtn = true;
                 this.activity = false;
            });
          });
    }
    public nowverify(){
      if(!this.verification)
      {
        TNSFancyAlert.showError(
          "Error!",
          "Please enter the code first.",
          "close"
        ).then(() => {

        });
      }
      else
      {
        this.activity = true;
        this.showbtn = false;
        var myinputted = appSettings.getString("mycode");
        if( myinputted !== this.verification )
        {
          //the code is not correct
          TNSFancyAlert.showError(
            "Error!",
            "the code you entered is not correct.",
            "close"
          ).then(() => {
               console.log("user pressed the button");
               this.counter = this.counter + 1;
               var num = 5;
               var attempt = num - this.counter;
               if(attempt === 0 || attempt < 1)
               {
                 //CONTINUE FROM HERE :delete the info
                 return this.credentials();
               }
               TNSFancyAlert.showInfo(
                 "Info!",
                 attempt + " attempts left.",
                 "close"
               ).then(() => {
                 this.showbtn = true;
                 this.activity = false;
               });
          });
        }
        else
        {
          //the code is correct procceed to the next page
          //the next page is the gallery page but first clean up before you continue
          TNSFancyAlert.showSuccess(
            "Success!",
            "correct code.",
            "close"
          ).then(() => {
            appSettings.remove("mycode");
            //navigate 
            //also we need to update the database before we procceed
            //time we used the email adress to  et the signature then delete email from the application settings;
            var checkemail = appSettings.getString("myemail");
            const mydata = {
              emailref : checkemail,
            }
            this.signuping.sign(mydata)
                .subscribe((res) => {
                  if(!res)
                  {
                    TNSFancyAlert.showError(
                      "Error!",
                      "Unknown error try again later.",
                      "close"
                    ).then(() => {
                      return this.credentials();
                    });
                  }
                  else if(res.message === "noemail")
                  {
                    TNSFancyAlert.showError(
                      "Error!",
                      "Please re-enter youre credentials.",
                      "close"
                    ).then(() => {
                      return this.credentials();
                    });
                  }
                  else if(res.messageverify === "errorverify")
                  {
                    TNSFancyAlert.showError(
                      "Error!",
                      "Account creation failed youre account was not verified but the code was correct try creating youre account again later.",
                      "close"
                    ).then(() => {
                      return this.credentials();
                    });
                  }
                  else
                  {
                    TNSFancyAlert.showSuccess(
                      "Success!",
                      "Account creation successful youre account was verified.",
                      "close"
                    ).then(() => {
                      var signature = res.message.toString();
                      appSettings.setString("idr" , signature);
                      appSettings.remove("myemail");
                      this.showbtn = true;
                      this.activity = false;
                      this.routerextensions.navigate(['/uploadpic'], {
                        clearHistory : true,
                        transition: {
                          name: 'slideLeft',
                          duration: 900,
                          curve: 'easeOut'
                        }
                      });
                    });
                  }
                },(error) => {
                  TNSFancyAlert.showError(
                    "Error!",
                    "Unknown error try again later.",
                    "close"
                  ).then(() => {
                    return this.credentials();
                  });
                });
          });
        }
      }
    }
}
