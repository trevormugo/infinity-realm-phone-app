import { Component, OnInit } from "@angular/core";
import { Page } from "tns-core-modules/ui/page";
import { RouterExtensions } from "nativescript-angular/router";
import { SignupService } from "../signup.service";
import { TNSFancyAlert, TNSFancyAlertButton } from "nativescript-fancyalert";
import { Adress } from "../ip.service";
import * as appSettings from "tns-core-modules/application-settings";

@Component({
    selector: "ns-signup",
    providers: [ Adress , SignupService ],
    moduleId: module.id,
    templateUrl: "./signup.component.html",
    styleUrls: ["./signup.component.css"],
})
export class SignupComponent implements OnInit {
    public activity:Boolean = false;
    public showbtn:Boolean = true;
    public email:String;
    public fullname:String;
    public nickname:String;
    public password:String;
    public phone:String;
    ngOnInit() {
        this.page.actionBarHidden = true;
        var checkcode = appSettings.getString("mycode");
        if(!checkcode)
        {
          //do nothing
        }
        else
        {
          appSettings.remove("mycode");
        }
    }
    constructor( public signuping : SignupService , public page : Page , public routerextensions : RouterExtensions) { }
    public goback(){
      if(this.activity === true)
      {
        TNSFancyAlert.showInfo(
          "info!",
          "wait till current process is finished.",
          "close"
        ).then(() => {

        });
      }
      else
      {
        this.routerextensions.back();
      }
    }
    public create(){
      if(!this.email || !this.phone || !this.fullname || !this.nickname || !this.password)
      {
        //fill in all the fields
        TNSFancyAlert.showError(
          "Error!",
          "Please fill in all the respective fields.",
          "close"
        ).then(() => {
        });
      }
      else
      {
        this.showbtn = false;
        this.activity = true;
        const infop = {
          email: this.email,
          fullname: this.fullname,
          nickname: this.nickname,
          phone: this.phone,
          password: this.password,
        };
        this.signuping.createaccount(infop)
            .subscribe((data) =>{
                if(data.code === "not unique")
                {
                  TNSFancyAlert.showError(
                    "Error!",
                    "an email like that one already exists.",
                    "close"
                  ).then(() => {
                       this.showbtn = true;
                       this.activity = false;
                  });
                }
                else if(data.code === "usernamenot")
                {
                  TNSFancyAlert.showError(
                    "Error!",
                    "username already exists.",
                    "close"
                  ).then(() => {
                       this.showbtn = true;
                       this.activity = false;
                  });
                }
                else if(data.code === "error")
                {
                  TNSFancyAlert.showError(
                    "Error!",
                    "something went wrong the code was not sent try agin later.",
                    "close"
                  ).then(() => {
                       this.showbtn = true;
                       this.activity = false;
                  });
                }
                else if(!data)
                {
                  TNSFancyAlert.showError(
                    "Error!",
                    "something went wrong.",
                    "close"
                  ).then(() => {
                       this.showbtn = true;
                       this.activity = false;
                  });
                }
                else if(data.code === "errordata")
                {
                  TNSFancyAlert.showError(
                    "Error!",
                    "something went wrong you're information was not saved try agin later.",
                    "close"
                  ).then(() => {
                       this.showbtn = true;
                       this.activity = false;
                  });
                }
                else
                {
                  TNSFancyAlert.showSuccess(
                    "Success",
                    "We sent you a verification code to youre email adress.",
                    "close"
                  ).then(() => {
                       this.showbtn = true;
                       this.activity = false;
                       var myString = data.code.toString();
                       var myEmail = this.email.toString();
                       appSettings.setString("mycode" , myString);
                       appSettings.setString("myemail" , myEmail);
                       appSettings.setString("cache" , "verifyemail");
                       this.routerextensions.navigate(['/verifyemail'], {
                         transition: {
                           name: 'slideLeft',
                           duration: 900,
                           curve: 'easeOut'
                         }
                       });
                  });
                }
            },(error)=>{
              TNSFancyAlert.showError(
                "Error!",
                "An error occured please try again later.",
                "close"
              ).then(() => {
                   this.activity = false;
                   this.showbtn = true;
              });
            });
      }
    }
}
