import { Component, OnInit } from "@angular/core";
import { Page } from "tns-core-modules/ui/page";
@Component({
    selector: "ns-error",
    providers: [],
    moduleId: module.id,
    templateUrl: "./error.component.html",
    styleUrls: ["./error.component.css"],
})
export class ErrorPageComponent implements OnInit {
    ngOnInit() {
      this.page.actionBarHidden = true;
    }
    constructor(public page : Page) {

    }
}
