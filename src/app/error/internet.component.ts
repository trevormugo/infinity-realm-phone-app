import { Component, OnInit } from "@angular/core";
import { Page } from "tns-core-modules/ui/page";
@Component({
    selector: "ns-internet",
    providers: [],
    moduleId: module.id,
    templateUrl: "./internet.component.html",
    styleUrls: ["./internet.component.css"],
})
export class InternetPageComponent implements OnInit {
    ngOnInit() {
      this.page.actionBarHidden = true;
    }
    constructor(public page : Page) {

    }
}
