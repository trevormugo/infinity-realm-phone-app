import { Pipe, PipeTransform } from '@angular/core';

@Pipe({name: 'timestr'})
export class TimeStr implements PipeTransform {
  transform(value: any): any {
    //let newStr: string = "";
    //in milliseconds
    let elapsedtime = Date.now() - value;
    let seconds = Math.floor(elapsedtime * 0.001);
    let minutes = Math.floor(seconds / 60);
    let hours = Math.floor(minutes / 60);
    let days = Math.floor(hours / 24);
    let weeks = Math.floor(days / 7);
    let months = Math.floor(weeks / 4);
    let years = Math.floor(months / 12);
    if(seconds >= 60)
    {
      if(minutes >= 60)
      {
        if(hours >= 24)
        {
          if(days >= 7)
          {
            if(weeks >= 4)
            {
              if(months >= 12)
              {
                if(years > 1)
                {
                  return years + " years ago";
                }
                else
                {
                  return years + " year ago";
                }
              }
              else
              {
                if(months > 1)
                {
                  return months + " months ago";
                }
                else
                {
                  return months + " month ago";
                }
              }
            }
            else
            {
              if(weeks > 1)
              {
                return weeks + " weeks ago";
              }
              else
              {
                return weeks + " week ago";
              }
            }
          }
          else
          {
            if(days > 1)
            {
              return days + " days ago";
            }
            else
            {
              return days + " day ago";
            }
          }
        }
        else
        {
          if(hours > 1)
          {
            return hours + " hours ago";
          }
          else
          {
            return hours + " hour ago";
          }
        }
      }
      else
      {
        if(minutes > 1)
        {
          return minutes + " minutes ago";
        }
        else
        {
          return minutes + " minute ago";
        }
      }
    }
    else
    {
      if(seconds > 1)
      {
        return seconds + " seconds ago";
      }
      else
      {
        return seconds + " second ago";
      }
    }
  }
}
